<?php
/*
 * Questo file è stato creato il 14-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js\"></script>";
$head .= "<script type=\"text/javascript\" src=\"js/profile-edit.js\"></script>";
require_once 'autoload.php';
$pagetitle = TITLE_PROFILE_USER_EDIT;
include 'header.php';
?>
<h1><?php echo TITLE_PROFILE_USER_EDIT ?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

$user->hideAllColumns();
$user->showColumns(array('nome','cognome','email', 'id_tipoutente', 'id_sex', 'dt_nascita', 
    'codicefiscale', 'codicerui', 'documento_identita'));
$user->setColumnHtmlType("id_tipoutente", GUI::HTML_SELECT);
$user->setColumnHtmlType("id_sex", GUI::HTML_SELECT);
$user->setColumnHtmlType("documento_identita", GUI::FILE_UPLOAD);
//$user->setColumnHtmlType("biennio", GUI::BIENNIUM);
?>
<form name="frmProfile" id="frmProfile" action="do_utente.php" method="post">
<?php
echo GUI::formFromEntity($user, 'class="profile"');
?>
    <input type="hidden" name="action" id="frmaction" value="save"/>
    <button type="submit" name="btnSaveProfile" id="btnSaveProfile"><?php echo LABEL_SAVE_EDITS?></button>
</form>    
<?php
include 'footer.php';
?>