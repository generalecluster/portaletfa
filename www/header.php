<!DOCTYPE html>
<?php //error_log("header included by ".$_SERVER['SCRIPT_FILENAME']);
if (!isset($pagedivid)) {
    $pagedivid = "page";
}
?>
<html dir="LTR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.6/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.6/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.js"></script>
    <?php echo $head ?>

        <title><?php echo $pagetitle ?></title>
    </head>
    <body>
        <!-- page start -->
        <div id="<?php echo $pagedivid?>">
            <!-- header start -->
            <div id="header">
                <!--Helpdesk e supporto tecnico via email: <a href="mailto:info@tfalegal.it">info@tfalegal.it</a>-->
                <div class="clear"></div>
            <!-- header end -->
            </div>

            <?php if (!empty($user)) { ?>
            <nav>
                <ul>
                    <li id="navuserdata"><?php echo LABEL_WELCOME.": ".$user->nome." ".$user->cognome."&nbsp;(".$user->codicefiscale.")";?></li>
                    <li><a id="navhome" href="index.php"><?php echo LABEL_HOME_PAGE?></a></li>                    
                    <li><a id="navuserprofile" href="userprofile.php"><?php echo TITLE_PROFILE_USER ?></a></li>
                    <li><a id="navlogout" href="do_logout.php">Logout</a></li>                    
                </ul>
            </nav>
            <?php }?>

            <div class="clear"></div>
            <!-- main start -->
            <div id="main">