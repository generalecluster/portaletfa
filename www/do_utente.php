<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
if (isset($_POST['action']) && $_POST['action'] == 'save') {
    $reserved = true;
} else {
    $reserved=false;
}
require_once 'autoload.php';
require_once 'classes/phpMailer/PHPMailerAutoload.php';

$dbg = new Debug();
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);
$dbg->verbose=false;

$dbg->outln("entered do_utente");

$entity = new Utente();
if ($_POST['action'] == "dologin") {
    $user = new Utente("","");
    $userlist = $user->getBy(array("username" => $_POST['username'], "password" => md5($_POST['password'])));
    if ($userlist[0]->id_utente != "") {
//        session_start();
        $_SESSION['user'] = $userlist[0]->id_utente;
        header('Location: index.php');
        //echo "OK";
    } else {
//        session_start();
        $_SESSION['errors'][] = ERROR_INVALID_USERNAME_OR_PASSWORD;
        //echo ERROR_INVALID_USERNAME_OR_PASSWORD;
        header('Location: login.php');
    }
} else if ($_POST['action'] == "add") {
        require_once(__DIR__.'/classes/log4php/Logger.php');
        
        // Tell log4php to use our configuration file.
        Logger::configure(__DIR__.'/config.xml');

        // Fetch a logger, it will inherit settings from the root logger
        $log = Logger::getLogger('DasRegister');
    try {
        $log->debug(print_r($_POST,1));
        //check input values before saving user
//        if (!preg_match("/[^a-zA-Z0-9]+/", $_POST['password'])) {
//            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
//        }
        if ($_POST["email"] != $_POST["email2"]) {
            throw new Exception(ERROR_EMAIL_CONFIRM_WRONG);
        }
        
        if (strlen($_POST['password'])<8 || strlen($_POST['password'])>20) {
            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
        }
//        if (!preg_match("/[A-Z]+/", $_POST['password'])) {
//            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
//        }
//        if (!preg_match("/[0-9]+/", $_POST['password'])) {
//            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
//        }
        //get current datetime
        $now = date("Y-m-d H:i:s");
        //assign POST values to corresponding entity properties
        $entity->parsePostValues($_POST);
        $entity->id_tipoformazione = 2; //aggiornamento. At the moment, all users are in Aggiornamento type.
        //assign properties which do not have direct correspondence in the POST.
        $entity->username = strtolower($_POST['codicefiscale']);
        $entity->password = md5($_POST['password']);
        $entity->id_stato = 1; //1= waiting for confirmation
        //prepare the token to verify the registration confirmation via email
        $entity->tokentime = $now;
        $entity->token = hash('sha512', $entity->username.$entity->tokentime);
        if ($_POST['id_tipoutente'] == "") {
            $entity->id_tipoutente = $_POST['id_tipoutente2'];
        }
        //set biennium to 0 by default, because the biennium will be set on every user access.
        //Now changed: biennium is set on registration and may be modified from the profile page.
        $entity->biennio = $_POST['biennio'];
        
        //error_log("*************utente biennio: ".$entity->biennio);
        //save user to db
        $userid = $entity->insert();
        $entity->getByPrimaryKey($userid);
        //error_log("entity tipoutente in do_utente: ". $entity->descrizione_tipoutente);
        //save user on moodle
        $client = LmsClientFactory::getInstance();
        //send password in clear to moodle
        $entity->password = substr($_POST['password'],0,16)."A1*";
        $result = $client->createUser($entity);
        //transform the json response from the LMS in a php object 
        //$result = json_decode($result);
        $log->debug($result);
        //manage errors
        if (!empty($result->exception)) {
            throw new Exception(ERROR_REMOTE_SERVER_CALL.":".$result->exception." - ".$result->errorcode." - ".$result->debuginfo);
        }
        
        //send confirmation email to the user
        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $config->mailserver;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $config->mailuser;                 // SMTP username
        $mail->Password = $config->mailpass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
     
        $mail->setFrom('noreply@tfalegal.it', 'Registrazione DAS');
        $mail->addAddress($entity->email, trim($entity->nome." ".$entity->cognome));     // Add a recipient

        $mail->isHTML(true);                                  // Set email format to HTML

        $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
        $token = $entity->token;
        $mail->Subject = EMAIL_REGISTRATION_REQUEST_TITLE;
        $mail->Body    = sprintf(EMAIL_REGISTRATION_REQUEST_BODY_HTML, trim($entity->nome." ".$entity->cognome), $root."register_confirm.php?t=".$token."&u=".$userid);
        $mail->AltBody = sprintf(EMAIL_REGISTRATION_REQUEST_BODY, trim($entity->nome." ".$entity->cognome), $root."register_confirm.php?t=".$token."&u=".$userid);

        if(!$mail->send()) {
            throw new Exception(ERROR_EMAIL_SENDING." : ".$mail->ErrorInfo);
        }
        $_SESSION['messages'][] = LABEL_OPERATION_OK;
        $_SESSION['messages'][] = MESSAGE_WILL_SEND_CONFIRM_EMAIL;
        header('Location: success.php');
    } catch (Exception $oEx) {
        $log->fatal(ERROR_DATA_INSERT. ": ".$oEx->getMessage());
        $_SESSION['errors'][] = ERROR_DATA_INSERT. ": ".$oEx->getMessage();
        $_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
} else if (isset($_POST['action']) && $_POST['action'] == 'save') {
    try {
        //get original user data from db
        $origuser = new Utente();
        $origuser->getByPrimaryKey($user->id_utente);
        //assign posted value to the current logged-in user
        $user->parsePostValues($_POST);
        // update user on the db
        $user->update();
        $user->getByPrimaryKey($user->id_utente);
        $user->password = substr($user->password,0,16)."A1*";
        //get lms client interface to update user data on the lms
        $client = LmsClientFactory::getInstance();
        //send fake password in clear to moodle
        //$entity->password = substr($_POST['password'],0,16)."A1*";
        //update user data on the lms 
        $result = $client->updateUser($user);
        //transform the json response from the LMS in a php object 
        //$result = json_decode($result);
        
        //manage errors
        if (!empty($result->exception)) {
            throw new Exception(ERROR_REMOTE_SERVER_CALL.":".$result->exception." - ".$result->errorcode." - ".$result->debuginfo);
        }
        
        //check if biennium is being changed
        if ($origuser->biennio !== $user->biennio) {
            //if biennium has been changed, update all credits references for exams taken in the current year
            $exam = new UtenteUnitadidattica();
            $sql = "update ".$exam->dbtablename." SET biennio=:biennio WHERE id_utente=:id_utente AND year(dt_superamento)=".date('Y');
            if ($exam->execSQL($sql, array("biennio" => $user->biennio, "id_utente" => $user->id_utente)) === FALSE) {
                throw new Exception("error modifying biennio");
            }
        }
           
        $_SESSION['messages'][] = LABEL_OPERATION_OK;
        header('Location: userprofile.php');
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = ERROR_DATA_INSERT. ": ".$oEx->getMessage();
        $_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
} else if (isset($_POST['action']) && $_POST['action'] == 'saveadm') {
     try {
        if ($user->id_ruolo != 1) {
            throw new Exception(ERROR_USER_UNAUTHORIZED);
        }
        //get original user data from db
        $origuser = new Utente();
        $origuser->getByPrimaryKey($user->id_utente);
        $entity = new Utente();
        //assign posted value to the current logged-in user
        $entity->parsePostValues($_POST);
        // update user on the db
        $entity->update();
        $entity->getByPrimaryKey($entity->id_utente);
        $entity->password = substr($entity->password,0,16)."A1*";
        //get lms client interface to update user data on the lms
        $client = LmsClientFactory::getInstance();
        //send fake password in clear to moodle
        //$entity->password = substr($_POST['password'],0,16)."A1*";
        //update user data on the lms 
        $result = $client->updateUser($entity, true);
        //transform the json response from the LMS in a php object 
        $result = json_decode($result);
        
        //manage errors
        if (!empty($result->exception)) {
            throw new Exception(ERROR_REMOTE_SERVER_CALL.":".$result->exception." - ".$result->errorcode." - ".$result->debuginfo);
        }
        
        //check if biennium is being changed
        if ($origuser->biennio !== $entity->biennio) {
            //if biennium has been changed, update all credits references for exams taken in the current year
            $exam = new UtenteUnitadidattica();
            $sql = "update ".$exam->dbtablename." SET biennio=:biennio WHERE id_utente=:id_utente AND year(dt_superamento)=".date('Y');
            if ($exam->execSQL($sql, array("biennio" => $entity->biennio, "id_utente" => $entity->id_utente)) === FALSE) {
                throw new Exception("error modifying biennio");
            }
        }
        
        $_SESSION['messages'][] = LABEL_OPERATION_OK;
        header('Location: admin/userprofile.php?id='.$entity->id_utente);
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = ERROR_DATA_INSERT. ": ".$oEx->getMessage();
        $_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
} else if (isset($_POST['action']) && $_POST['action'] == 'reqpwd') {
    if (isset($_POST['cf'])) {
        $userlist = $entity->getBy(array("codicefiscale" => $_POST['cf']));
        if (count($userlist) > 0) {
            $entity->getByPrimaryKey($userlist[0]->id_utente);
            //prepare a token for password recovery
            //$pwdtoken = substr(hash('sha256', $user->id_utente.time()), 0, 16);
            $pwdtoken = hash('sha256', $entity->id_utente.time());
            $now = date("Y-m-d H:i:s");
            $entity->pwdtoken = $pwdtoken;
            $entity->pwdtokentime = $now;
            $entity->update();
            $config = new Config();
            $mailer = Mailer::getInstance();
            $mail = $mailer->mailer;
            //$mail->SMTPDebug = SMTP::DEBUG_CONNECTION;
            $mail->addAddress($entity->email, trim($entity->nome." ".$entity->cognome));     // Add a recipient
            //$mail->addAddress('softmasters@gmail.com', "Alex Laudani");     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
            $mail->Subject = EMAIL_RECOVER_PASSWORD_TITLE;
            $mail->Body    = sprintf(EMAIL_RECOVER_PASSWORD_BODY_HTML, trim($entity->nome." ".$entity->cognome), $root."password-change.php?t=".$pwdtoken);
            $mail->AltBody = sprintf(EMAIL_RECOVER_PASSWORD_BODY, trim($entity->nome." ".$entity->cognome), $root."password-change.php?t=".$pwdtoken);
            $mailer->sendMail();
            $_SESSION['messages'][] = LABEL_OPERATION_OK;
            $_SESSION['messages'][] = MESSAGE_PASSWORD_REQUEST_EMAIL_SENT;
            header('Location: success.php');
        } else {
            $_SESSION['errors'][] = ERROR_PARAMETERS_BAD.": cf";
            header('Location: error.php');
        }
    }
} else if (isset($_POST['action']) && $_POST['action'] == 'changepwd') {
    if (!isset($_POST['pwd1']) || !isset($_POST['pwd2'])) {
        $_SESSION['errors'][] = ERROR_PASSWORD_NOT_EQUAL;
        //$_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else if ($_POST['pwd1'] != $_POST['pwd2']) {
        $_SESSION['errors'][] = ERROR_PASSWORD_NOT_EQUAL;
        //$_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else if (strlen($_POST['pwd1'])<8 || strlen($_POST['pwd1'])>20) {
        $_SESSION['errors'][] = ERROR_PASSWORD_BAD_FORMAT;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    } else if (!isset($_POST['t'])){
        $_SESSION['errors'][] = ERROR_PARAMETERS_BAD.": token";
        header('Location: error.php');
    } else {
        $userdata = null;
        $userlist = $entity->getBy(array("pwdtoken" => $_POST['t']));
        if (count($userlist) > 0) {
            $userdata = $userlist[0];
            //check if token has expired
            $tokentime = $userdata->pwdtokentime;
            $now = date(DBConn::$mysqlDateFormat);
            $tokenage = StrUtils::dateDifference($tokentime, $now, '%h');
            //token expires after 24 hours
            if ($tokenage >= 24) {
                $_SESSION['errors'][] = ERROR_WRONG_TOKEN;
                header('Location: error.php');
            } else {
                $entity->id_utente = $userdata->id_utente;
                $entity->password = md5($_POST['pwd1']);
                if ($userdata->id_stato == 1) {//if user is waiting for confirmation, set as confirmed
                    $entity->id_stato = 4;
                }
                $entity->update();
                $_SESSION['messages'][] = LABEL_OPERATION_OK;
                $_SESSION['messages'][] = MESSAGE_PASSWORD_CHANGED;
                header('Location: success.php');
            }
        }
    }
} else if (isset($_GET['t'])) {//user activation from link in the email
    try {
        if (isset($_GET['u'])) {
            $entity = new Utente();
            $entity->getByPrimaryKey($_GET['u']);
            //checks for token expiration
            if ($entity->token === $_GET['t']) {
                $tokendate = $entity->tokentime;
                $elapsed = StrUtils::dateDifference($tokendate, date('Y-m-d H:i:s'), '%s');
                if ($elapsed < 60*60*24) {//the token expires after 24 hours
                    $entity->id_stato=4;//Active
                    $entity->update();
                    $_SESSION['messages'][] = MESSAGE_USER_ACTIVATED;
                    header('Location: login.php');
                } else {
                    throw new Exception(ERROR_WRONG_TOKEN);
                }
            } else {
                throw new Exception(ERROR_WRONG_TOKEN);
            }
        }
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = $oEx->getMessage();
        header('Location: error.php');
    }
} else if ($_POST['action'] == 'cf') {
    $cf = new CodiceFiscale();
    $cf->SetCF($_POST['cf']);
    if ($cf->GetCodiceValido()) {
//        //check comune nascita
//        $comune = new Comune();
//        $comune->getByPrimaryKey($_POST['com']);
//        if ($cf->GetComuneNascita() != $comune->cod_catasto) {
//            echo "\"Il Comune di nascita non corrisponde.\"";
//            return;
//        }
        echo "\"true\"";
    } else {
        echo "\"".$cf->GetErrore()."\"";
    }
} else if (isset($_GET['action']) && $_GET['action'] == "getUsersTable") {
    $entity->start = (int)$_GET['start'];
    $entity->pagesize = (int)$_GET['length'];
    $entity->sqlwhere = " WHERE a.id_stato <> ".Stato::DELETED;
    $entity->setOrderBy($_GET['columns'], $_GET['order']);
    $data = $entity->getData();
    $result = new stdClass();
    $result->draw = (int)$_GET['draw'];
    $result->recordsTotal = $data->records;
    $result->recordsFiltered = $data->records;
    $result->data = $data->rows;
    echo json_encode($result);
}
?>