<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$reserved = TRUE;
require_once 'autoload.php';
if (isset($user) && !empty($user->id_utente)) {
    if ($user->id_tipoformazione == 1) {
        header("Location: index-formazione.php");
        exit();
    } else if ($user->id_tipoformazione == 2) {
        header("Location: index-aggiornamento.php");
        exit();
    } else {
        $_SESSION['errors'][] = ERROR_USER_TYPE_UNEXPECTED;
        header("Location: error.php");
        exit();
    }
}

