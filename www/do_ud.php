<?php
/*
 * Questo file è stato creato il 11-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved=true;
require_once 'autoload.php';
require_once 'classes/phpMailer/PHPMailerAutoload.php';

$dbg = new Debug();
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);
$dbg->verbose=false;

$dbg->outln("entered do_ud");

$entity = new Unitadidattica();
if ($_POST['action'] == "sub") {
    try {
        //retrieve learning unit
        if (isset($_POST['ud'])) {
            $entity->getByPrimaryKey($_POST['ud']);
        }
        if (empty($entity->id_unitadidattica)) {
            throw new Exception(ERROR_NOT_FOUND." :".$_POST['ud'], 404);
        }
        //TODO: make checks on learning unit (suspended, deleted, allowed, a.s.o.)

        //add user to learning unit.
        //look for user in session
        if (!empty($user->id_utente)) {
            $udutente = new UtenteUnitadidattica();
            //check if already subscribed
            $udutentelist = $udutente->getBy(array('id_utente' => $user->id_utente, 'id_unitadidattica' => $entity->id_unitadidattica));
            if (count($udutentelist) > 0) {
                //user already subscribed
                throw new Exception(ERROR_ALREADY_SUBSCRIBED, 409);
            } else {
                //if not subscribe, subscribe on local database
                $udutente->id_utente = $user->id_utente;
                $udutente->id_unitadidattica = $entity->id_unitadidattica;
                $udutente->id_tipoformazione = $user->id_tipoformazione;
                //do save effectively
                $id_new = $udutente->insert();
                //populate the entity with the last insert id
                $udutente->id_utente_unitadidattica = $id_new;
                
                //now subscribe the user on the lms platform too
                $oClient = LmsClientFactory::getInstance();
                try {
                    $response = $oClient->selfEnrolUser($user, $entity->id_extern);
                    $response = json_decode($response);
                } catch (Exception $oEx) {
                    //maybe the user does not exist on the lms. Try to create the user.
                    try {
                        $user->password = $user->password."A1*";
                        $result = $oClient->createUser($user);
                    } catch (Exception $oExx) {
                        $udutente->delete($udutente->id_utente_unitadidattica);
                        throw new Exception("error enrolling user. id_utente_unitadidattica: ".$udutente->id_utente_unitadidattica." - ".$oEx->getMessage()." - ".$oEx->getTraceAsString(), $oEx->getCode());
                    }
                }
                //return the entity so created, in json format
                echo $udutente->renderJson();
            }
        } else {
            throw new Exception(ERROR_NOT_LOGGED_IN, 403);
        }
    } catch (Exception $oEx) {
        http_response_code($oEx->getCode()==0 ? 500 : $oEx->getCode());
        header('Content-Type: application/json');
        $res = new stdClass();
        $res->code = $oEx->getCode();
        $res->message = $oEx->getMessage();
        error_log($oEx->getMessage()."\n".$oEx->getTraceAsString());
        echo json_encode($res);        
    }
} else if ($_POST['action'] == "add") {
    try {
        //check input values before saving user
        if (!preg_match("/[^a-zA-Z0-9]+/", $_POST['password'])) {
            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
        }
        if (strlen($_POST['password'])<6 || strlen($_POST['password'])>20) {
            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
        }
        if (!preg_match("/[A-Z]+/", $_POST['password'])) {
            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
        }
        if (!preg_match("/[0-9]+/", $_POST['password'])) {
            throw new Exception(ERROR_PASSWORD_BAD_FORMAT);
        }
        //get current datetime
        $now = date("Y-m-d H:i:s");
        //assign POST values to corresponding entity properties
        $entity->parsePostValues($_POST);
        //assign properties which do not have direct correspondence in the POST.
        $entity->username = strtolower($_POST['codicefiscale']);
        $entity->password = md5($_POST['password']);
        $entity->id_stato = 1; //1= waiting for confirmation
        //prepare the token to verify the registration confirmation via email
        $entity->tokentime = $now;
        $entity->token = hash('sha512', $entity->username.$entity->tokentime);
        
        //save user to db
        $userid = $entity->insert();
        
        //save user on moodle
        $client = LmsClientFactory::getInstance();
        //send password in clear to moodle
        $entity->password = $_POST['password'];
        $result = $client->createUser($entity);
        //transform the json response from the LMS in a php object 
        $result = json_decode($result);
        
        //manage errors
        if (!empty($result->exception)) {
            throw new Exception(ERROR_REMOTE_SERVER_CALL.":".$result->exception." - ".$result->errorcode." - ".$result->debuginfo);
        }
        
        //send confirmation email to the user
        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $config->mailserver;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $config->mailuser;                 // SMTP username
        $mail->Password = $config->mailpass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
     
        $mail->setFrom('noreply@tfalegal.it', 'Registrazione DAS');
        $mail->addAddress($entity->email, trim($entity->nome." ".$entity->cognome));     // Add a recipient

        $mail->isHTML(false);                                  // Set email format to HTML

        $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
        $token = $entity->token;
        $mail->Subject = EMAIL_REGISTRATION_REQUEST_TITLE;
        $mail->Body    = sprintf(EMAIL_REGISTRATION_REQUEST_BODY, trim($entity->nome." ".$entity->cognome), $root."/register_confirm.php?t=".$token."&u=".$userid);
        $mail->AltBody = $mail->Body;

        if(!$mail->send()) {
            throw new Exception(ERROR_EMAIL_SENDING." : ".$mail->ErrorInfo);
        }
        $_SESSION['messages'][] = LABEL_OPERATION_OK;
        $_SESSION['messages'][] = MESSAGE_WILL_SEND_CONFIRM_EMAIL;
        header('Location: success.php');
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = ERROR_DATA_INSERT. ": ".$oEx->getMessage();
        $_SESSION['postvars'] = $_POST;
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
} else if (isset($_GET['t'])) {
    try {
        if (isset($_GET['u'])) {
            $entity = new Utente();
            $entity->getByPrimaryKey($_GET['u']);
            //checks for token expiration
            if ($entity->token === $_GET['t']) {
                $tokendate = $entity->tokentime;
                $elapsed = StrUtils::dateDifference($tokendate, date('Y-m-d H:i:s'), '%s');
                if ($elapsed < 60*60*24) {
                    $entity->id_stato=4;//Active
                    $entity->update();
                    $_SESSION['messages'][] = MESSAGE_USER_ACTIVATED;
                    header('Location: login.php');
                } else {
                    throw new Exception(ERROR_WRONG_TOKEN);
                }
            } else {
                throw new Exception(ERROR_WRONG_TOKEN);
            }
        }
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = $oEx->getMessage();
        header('Location: error.php');
    }
}
?>