<?php
/*
 * Questo file è stato creato il 18-mar-2013 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = false;
$head = <<<EOT
<script type="text/javascript">
    //alert("Il portale della formazione è momentaneamente non disponibile per interventi di aggiornamento");
        //ricordarsi di sistemare action
</script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>        
<script type="text/javascript" src="js/login.js"></script>
<style>
    body{background:url('img/login-bg.jpg') top center no-repeat;}
    #page{height: 642px;position: relative;}
    #header{background:none;}
    h1{display:none;}
    label.hidden{display:none}
    label.lblrd {float:none}
    label.error {color:red; background:#fbc2c4; border:1px solid red;padding:4px; display:block; width:185px}
    input {color:#000}
    #loginwrapper{width:1024px;margin-left:auto;margin-right:auto; position:relative;}
    #frmLogin{position:absolute;top:30px; right:26px; width:292px; padding-top:15px; background:rgb(0,57,111); color:white}
    #username{margin-bottom:15px;}
    #password{margin-bottom:10px;}
    #btnlogin{margin-left:0; margin-bottom:10px;}
    #messages{width:240px;position:absolute;right:325px; top:45px; color:red; font-size:0.9em; font-weight:bold;}
    .footercontactinfo {width:1024px; margin-left:auto; margin-right:auto; text-align:center;}
   a {color:white}
   a:link {text-decoration:underline}
   a:visited {text-decoration:underline}
   a:hover {text-decoration:none}
   a:active {text-decoration:underline}
   div.selectbiennium {font-size:0.9em; font-style:italic}
</style>
EOT;
require_once 'autoload.php';
$pagetitle = TITLE_LOGIN_PAGE;
$pagedivid = "pagelogin";
include 'header.php';
?>
<div id="loginwrapper">
<h1><?php echo TITLE_LOGIN_PAGE?></h1>
<div id="messages">
<?php

if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

if (isset($_SESSION['messages'])) {
    GUI::showErrors($_SESSION['messages']);
    $_SESSION['messages'] = array();
}
?>
</div>
<form name="frmLogin" id="frmLogin" action="do_login.php" method="post">
    <ul id="listlogin">
        <li><label for="username" id="lblusername" class="lbltext hidden"><?php echo LABEL_USERNAME."(".LABEL_SSN_NUMBER.")"?></label><input class="inputtext" type="text" id="username" name="username" placeholder="<?php echo LABEL_SSN_NUMBER?>"/></li>
        <li><label for="password" id="lblusername" class="lbltext hidden"><?php echo LABEL_PASSWORD?></label><input class="inputtext" type="password" id="password" name="password" placeholder="<?php echo LABEL_PASSWORD?>"/></li>
        <!--li>
            <input type="radio" name="rdlearningtype" id="rdlearninginitial" value="1"/>
            <label for="rdlearninginitial" class="lblrd"><?php echo LABEL_INITIAL_TRAINING?></label>
        </li-->
        <!--li>
            <div><?php echo LABEL_SELECT_LEARNING_NEEDS?></div>
            <input type="radio" name="rdlearningtype" id="rdlearningadvanced" value="2"/>
            <label for="rdlearningadvanced" class="lblrd"><?php echo LABEL_ADVANCED_TRAINING?></label>
        </li>
        <li class="biennium">
            <div class="selectbiennium"><?php echo LABEL_SELECT_BIENNIUM?></div>
            <input type="radio" name="rdbiennium" id="rdbiennium1" value="<?php echo date("Y")-1?>"/>
            <label for="rdbiennium1" class="lblrd"><?php echo (date('Y')-1)."/".date('Y')?></label>
        </li>
        <li class="biennium">
            <input type="radio" name="rdbiennium" id="rdbiennium2" value="<?php echo date("Y")?>"/>
            <label for="rdbiennium2" class="lblrd"><?php echo date('Y')."/".(date('Y')+1)?></label>
        </li-->
        <li>
            <input type="button" name="btnlogin" id="btnlogin" value="<?php echo LABEL_SEND?>"/>
            <input type="hidden" name="action" id="action" value="dologin"/>
        </li>
        <li class="notregistered">Non sei registrato? <a href="register.php">Registrati</a></li>
        <li class="notregistered"><a href="password-recover.php">Recupera password</a></li>
    </ul>
</form>
</div>
<?php
include 'footer.php';
?>
