<?php



//dati utente
define('LABEL_DT_BIRTH',"Data di nascita");
define('LABEL_AGENCY_CODE',"Codice Agenzia");
define('LABEL_ASK_HAVE_RUI_CODE',"Hai un codice RUI?");
define('LABEL_BIENNIUM', "Biennio");
define('LABEL_RUI_CODE',"Codice RUI");
define('LABEL_CURRENTLY_UPLOADED_FILE', "File attualmente caricato");
define('LABEL_DAS_CODE',"Codice Collaborazione DAS");
define('LABEL_DATE_OF_BIRTH',"Data di nascita");
define('LABEL_EMAIL', "E-mail");
define('LABEL_EMAIL_CONFIRM', "Conferma e-mail");
define('LABEL_HOME_PAGE', 'Pagina iniziale');
define('LABEL_IDENTITY_CARD', 'Documento di identità');
define('LABEL_RECOVER_PASSWORD',"Recupera la tua password");
define('LABEL_SAVE_EDITS', "Salva modifiche");
define('LABEL_PASSWORD_NEW', "Nuova password");
define('LABEL_PASSWORD_NEW_CONFIRM', "Conferma nuova password");
define('LABEL_PROVINCE_OF_BIRTH',"Provincia di nascita");
define('LABEL_CITY_OF_BIRTH',"Comune di nascita");
define('LABEL_INSERT_RUI_CODE',"Inserisci il codice RUI");
define('LABEL_PRIVACY', "Ho letto ed accetto <a href=\"privacy.html\" target=\"_new\">l'informativa sulla privacy</a>");
define('LABEL_ROLE_SELECT',"Seleziona il tuo ruolo");
define('LABEL_RUI_NOT_REGISTERED',"Non iscritto RUI che esercita all'interno dei locali");
define('LABEL_SELECT_YOUR_ROLE', "Seleziona il tuo ruolo");
define('LABEL_SELECT_YOUR_WANNABE_ROLE', "Seleziona il ruolo che vuoi ottenere");
define('LABEL_USER_NOT_EXISTS_LMS', "L'utente non è presente sull'LMS");
define('LABEL_USER_UPDATE_LMS', "Salva nuovamente il profilo per aggiungere l'utente all'LMS");
define('LABEL_VAT_NUMBER', "Partita IVA");
define('MESSAGE_COMPLETE_PROFILE', "Grazie per esserti iscritto al portale. Per completare la registrazione ti chiediamo gentilmente di completare il tuo profilo con i dati indispensabili alla fruizione dei corsi.");
define('MESSAGE_INSERT_SSN_NUMBER', "Inserisci il tuo codice fiscale");
define('MESSAGE_PASSWORD_CHANGED', 'La password è stata modificata');
define('MESSAGE_PASSWORD_REQUEST_EMAIL_SENT', 'L\'email con le istruzioni per il recupero della password è stata inviata all\'indirizzo specificato durante la registrazione');
define('MESSAGE_SEND_RECOVER_PASSWORD', 'Invieremo le istruzioni per cambiare la password al tuo indirizzo email: %1$s (L\'indirizzo è camuffato per motivi di privacy)<br/>Fai clic su invia per ricevere le istruzioni via email');
define('MESSAGE_USER_ACTIVATED', "Il tuo account è stato confermato correttamente. Adesso puoi accedere al portale utilizzando il tuo codice fiscale e la password che hai scelto in fase di registrazione.");
define('MESSAGE_WILL_SEND_CONFIRM_EMAIL', "Per completare l'iscrizione devi cliccare sul link di conferma che riceverai nella tua casella email");
define('EMAIL_RECOVER_PASSWORD_TITLE', "DAS - Recupero password");
define('EMAIL_RECOVER_PASSWORD_BODY', 'Gentile %1$s,\nè stato chiesto di recuperare la password per il tuo account sul Portale della Formazione DAS. Per scegliere una nuova password fai clic sul seguente link e segui le istruzioni:\n%2$s');
define('EMAIL_RECOVER_PASSWORD_BODY_HTML', 'Gentile %1$s,<br/>è stato chiesto di recuperare la password per il tuo account sul Portale della Formazione DAS. Per scegliere una nuova password fai clic sul seguente link e segui le istruzioni:<br/><a href="%2$s">%2$s</a>');
define('EMAIL_REGISTRATION_REQUEST_TITLE', "DAS - Richiesta di registrazione sul Portale Formazione");
define('EMAIL_REGISTRATION_REQUEST_BODY', 'Gentile %1$s,\ngrazie per esserti registrato sul Portale della Formazione DAS.\nPer confermare la tua registrazione fai click sul seguente link: %2$s');
define('EMAIL_REGISTRATION_REQUEST_BODY_HTML', 'Gentile %1$s,<br/>grazie per esserti registrato sul Portale della Formazione DAS.<br/>Per confermare la tua registrazione fai click sul seguente link: <a href="%2$s">%2$s</a>');
define('TITLE_CHOOSE_NEW_PASSWORD', "Scegli la tua nuova password");
define('TITLE_CITY_OF_BIRTH',"Comune nascita");
define('TITLE_DAS_CODE',"Codice DAS");
define('TITLE_DATE_OF_BIRTH',"Data di nascita");
define('TITLE_IDENTITY_CARD', 'Documento di identità');
define('TITLE_POPULATION',"Popolazione");
define('TITLE_PROFILE_USER',"Profilo utente");
define('TITLE_PROFILE_USER_EDIT',"Modifica profilo utente");
define('TITLE_PROVINCE_ID',"ID Prov.");
define('TITLE_PROVINCE_OF_BIRTH',"Prov. nascita");
define('TITLE_RECOVER_PASSWORD',"Recupero password");
define('TITLE_REGION',"Regione");
define('TITLE_SEX',"Sesso");
define('TITLE_VAT_NUMBER',"P.IVA");

//login
define('LABEL_SELECT_LEARNING_NEEDS', "Quali sono le tue necessità?");
define('LABEL_INITIAL_TRAINING', "Corso di formazione");
define('LABEL_ADVANCED_TRAINING', "Corso di aggiornamento");
define('LABEL_SELECT_BIENNIUM', "La normativa prevede che l’aggiornamento venga svolto in un biennio totalizzando minimo 60 ore (di cui almeno 15 per anno). Indica per quale biennio accedi ai crediti formativi");

//label generiche
define('LABEL_YES',"Sì");
define('LABEL_NO',"No");
define('LABEL_OPERATION_OK', "Operazione effettuata correttamente.");
define('LABEL_WELCOME', "Utente attivo");
define('LABEL_DOWNLOAD_REPORT_EXCEL', "Scarica il report in formato Excel");

//label corsi
define('LABEL_COURSE_EDIT',"Modifica corso");
define('LABEL_COURSE_DETAILS',"Dettagli del corso");
define('LABEL_COURSES_GOTO', "Vai ai corsi");
define('LABEL_COURSE_GOTO', "Vai al corso");
define('LABEL_COURSES_LISTOF', "Elenco corsi");
define('LABEL_COURSE_SUBSCRIBE', "Iscriviti al corso");
define('LABEL_COURSES_LIST_VIEW', "Visualizza elenco corsi");

//titoli corsi
define('TITLE_AREA_AGGIORNAMENTO', "Area Aggiornamento");
define('TITLE_AREA_FORMAZIONE', "Area Formazione");
define('TITLE_COURSE_DETAILS', "Dettagli corso");
define('TITLE_COURSE_EDIT', "Modifica corso");
define('TITLE_CREDITS', "Crediti");
define('TITLE_LEARNING_UNIT_ID', "Id U.D.");
define('TITLE_LEARNING_UNIT', "U.D.");
define('TITLE_DT_PASSED', "Data superamento");
define('TITLE_ID_EXTERN', "ID esterno");
define('TITLE_THEMATIC_AREA_ID', "Area tematica");
define('TITLE_THEMATIC_AREA', "Area tematica");

//errori
define('ERROR_WRONG_TOKEN', "Impossibile completare l'operazione. Token errato o scaduto.");
define('ERROR_PARAMETERS_BAD', "Impossibile completare l'operazione, a causa di parametri errati o mancanti");
//define('ERROR_PASSWORD_BAD_FORMAT', "La password deve essere lunga tra 6 e 10 caratteri, deve contenere almeno 1 lettera maiuscola, almeno 1 numero e almeno 1 carattere non alfanumerico, ad esempio *,-, oppure #. ");
define('ERROR_PASSWORD_BAD_FORMAT', "La password deve essere lunga tra 8 e 20 caratteri");
define('ERROR_EMAIL_CONFIRM_WRONG', "I campi \"E-mail\" e \"Conferma e-mail\" devono essere uguali");
define('ERROR_MAIL_SENDING', "Errore durante l'invio dell'email di conferma");
define('ERROR_PASSWORD_NOT_EQUAL', "I campi password e conferma password devono essere uguali");
define('ERROR_SSN_ALREADY_EXISTS', "Esiste già un account registrato con questo codice fiscale");
define('ERROR_USER_UNAUTHORIZED', "Non sei autorizzato ad eseguire l'operazione richiesta");
define('ERROR_USER_NOT_CONFIRMED', "Account non ancora confermato. Segui le istruzioni nella email di conferma attivazione per abilitare il tuo account. Se non hai ricevuto l'email, fai click su \"Recupera password\"");
define('ERROR_USER_NOT_SUBSCRIBED_COURSE', "Non iscritto");
define('ERROR_USER_TYPE_UNEXPECTED', "Tipo utente non previsto");
define('ERROR_NOT_FOUND', "Elemento non trovato");
//titoli pagine
define('TITLE_ERROR', "Errore");

//titoli colonne
define('TITLE_STATUS', "Stato");

//Amministrazione
define('TITLE_ADMIN_PAGE', "Pagina amministrazione");
define('LABEL_LEARNERS_LISTOF', "Elenco utenti");


define ('TITLE_LOGIN_PAGE', "Pagina di accesso");
define('TITLE_MAIN_PAGE', "Pagina principale");
define('TITLE_CUSTOMERS', "Clienti");
define('TITLE_CUSTOMER', "Cliente");
define('TITLE_NEW_CUSTOMER', "Nuovo cliente");
define('TITLE_EDIT_CUSTOMER', "Modifica cliente");
define('TITLE_MANAGEMENT', "Gestione");
define('TITLE_SERVERS', "Servers");
define('TITLE_PACKAGES', "Packages");
define('TITLE_REGISTER_PAGE', "Pagina di registrazione utente");
define('TITLE_SELLING', "Vendite");

define('LABEL_NEW_CUSTOMER', "Nuovo cliente");
define('LABEL_SELEZIONARE', "Selezionare");
define('LABEL_SALVA', "Salva");
define('LABEL_PULISCI', "Pulisci");

define('LABEL_SEND', "Invia");

define('TITLE_CUSTOMERS_LIST', "Elenco clienti");
define('TITLE_CREATE_NEW_CUSTOMER', "Creazione nuovo Cliente");
define('TITLE_SHOW_CUSTOMER', "Mostra Cliente");
define('TITLE_CUSTOMER_DATA', "Dati cliente");
define('TITLE_RESOURCES_USAGE', "Utilizzo risorse");
define('TITLE_NAME', "Nome");
define('TITLE_LASTNAME', "Cognome");
define('TITLE_BIENNIUM', "Biennio");
define('TITLE_COMPANY', "Ragione Sociale");
define('TITLE_ID_USER', "Id Utente");
define('TITLE_USERNAME', "Nome utente");
define('TITLE_PASSWORD', "Password");
define('TITLE_ROLE', "Ruolo");
define('TITLE_ID_OWNER', "Id propr.");
define('TITLE_CONTACTNAME', "Contatto");
define('TITLE_ADDRESS', "Indirizzo");
define('TITLE_ZIP', "CAP");
define('TITLE_CITY', "Citt&agrave;");
define('TITLE_ID_PROVINCE', "Id prov.");
define('TITLE_ID_COUNTRY', "ID nazione");
define('TITLE_SSN_NUMBER', "Codice Fiscale");
define('TITLE_PHONE', "Telefono");
define('TITLE_PHONE2', "Telefono 2");
define('TITLE_EMAIL', "E-mail");
define('TITLE_EMAIL2', "E-mail 2");
define('TITLE_DISCOUNT', "Sconto");
define('TITLE_ID_VAT', "ID IVA");
define('TITLE_NOTES', "Note");
define('TITLE_DT_INS', "Data Creazione");
define('TITLE_DT_MOD', "Ultima modifica");
define('TITLE_TRAINING_TYPE', "Tipo formazione");
define('TITLE_RUI_CODE', "Cod. RUI");
define('TITLE_AGENCY_CODE', "Cod. Agenzia");
define('TITLE_USERTYPE', "Tipo utente");

define('TITLE_USER_BACKUP_SERVICES_LIST', "Elenco Package di Backup attivi");
define('TITLE_ID_USER_BACKUP_SERVICE', "ID");
define('TITLE_PACKAGE_NAME', "Nome package");
define('TITLE_DESCRIPTION', "Descrizione");
define('TITLE_DT_START', "Data inizio");
define('TITLE_DT_END', "Data scadenza");

define('TITLE_BACKUP_ACCOUNTS_LIST', "Elenco account di backup");
define('TITLE_ID_BACKUPACCOUNT', "ID");
define('TITLE_SERVERNAME', "Nome server");
define('TITLE_ID_CLIENTTYPE', "Client");
define('TITLE_CLIENTTYPE', "Tipo Client");
define('TITLE_ALIAS', "Alias");
define('TITLE_LOGINNAME', "Login");

define('TITLE_BACKUP_PACKAGES_LIST', "Elenco package di backup");
define('TITLE_CREATE_NEW_BACKUP_PACKAGE', "Nuovo package di backup");
define('TITLE_ID_PACKAGE_BACKUP', "ID");
define('TITLE_ID_TYPE', "ID tipo");
define('TITLE_QUOTA', "Quota (MB)");
define('TITLE_AGENTS', "Agenti");
define('TITLE_PRICE_MONTH', "Prezzo mensile");
define('TITLE_PRICE_YEAR', "Prezzo annuale");

define('TITLE_SERVERS_LIST', "Elenco dei server");
define('TITLE_CREATE_NEW_SERVER', "Crea nuovo server");
define('TITLE_EDIT_SERVER', "Modifica server");
define('TITLE_ID_SERVER', "ID");
define('TITLE_URL', "Indirizzo/URL");

define('TITLE_ORDERS_LIST', "Elenco ordini");
define('TITLE_ORDER_DETAILS', "Dettagli ordine");
define('TITLE_CREATE_NEW_ORDER', "Crea nuovo ordine");
define('TITLE_EDIT_ORDER', "Modifica ordine");
define('TITLE_ID_ORDER', "ID");
define('TITLE_NET_AMOUNT', "Imponibile");
define('TITLE_VAT_AMOUNT', "Importo IVA");
define('TITLE_AMOUNT', "Importo");
define('TITLE_DT_ORDER', "Data ordine");
define('TITLE_PROCESSED_ORDER', "Evaso");
define('TITLE_PACKAGE', "Package");
define('TITLE_BILLING_PERIOD', "Periodo di fatturazione");
define('TITLE_PRICE', "Prezzo");

define('LABEL_QUOTA_USAGE', "Utilizzo quota disco (MB)");
define('LABEL_AGENTS_USAGE', "Utilizzo agenti");
define('LABEL_NAME', "Nome");
define('LABEL_SURNAME', "Cognome");
define('LABEL_ID_USER', "Id utente");
define('LABEL_USERNAME', "Nome utente");
define('LABEL_PASSWORD', "Password");
define('LABEL_PASSWORD_CONFIRM', "Conferma password");
define('LABEL_ROLE', "Ruolo");
define('LABEL_ROLE_CLIENT', "Cliente");
define('LABEL_ROLE_RESELLER', "Rivenditore");
define('LABEL_ROLE_ADMIN', "Admin");
define('LABEL_ID_OWNER', "Id propr.");
define('LABEL_OWNER', "Proprietario");
define('LABEL_CONTACTNAME', "Contatto");
define('LABEL_ADDRESS', "Indirizzo");
define('LABEL_ZIP', "CAP");
define('LABEL_CITY', "Citt&agrave;");
define('LABEL_ID_PROVINCE', "Id prov.");
define('LABEL_PROVINCE', "Provincia");
define('LABEL_ID_COUNTRY', "ID nazione");
define('LABEL_COUNTRY', "Nazione");
define('LABEL_SSN_NUMBER', "Codice Fiscale");
define('LABEL_VAT_NUMBER', "Partita IVA");
define('LABEL_PHONE', "Telefono");
define('LABEL_PHONE2', "Telefono 2");

define('LABEL_EMAIL2', "E-mail 2");
define('LABEL_DISCOUNT', "Sconto");
define('LABEL_ID_VAT', "ID IVA");
define('LABEL_VAT', "IVA");
define('LABEL_NOTES', "Note");
define('LABEL_DT_INS', "Data Creazione");
define('LABEL_DT_MOD', "Ultima modifica");
define('LABEL_DT_START', "Data inizio");
define('LABEL_DT_END', "Data scadenza");
define('LABEL_DT_ORDER', "Data Ordine");
define('LABEL_ADD_ROW', "Aggiungi riga");

define('LABEL_CHOOSE_OPTION', "Scegli un'opzione");
define('LABEL_60HRS_DOING', "Sto facendo le 60 ore iniziali");
define('LABEL_60HRS_DONE', "Ho già fatto le 60 ore iniziali");

define('LABEL_QUOTA', "Quota disco");
define('LABEL_AGENTS', "Numero di agenti");

define('LABEL_URL', "Indirizzo/URL");

define('TITLE_CREATE_NEW_BACKUP_ACCOUNT', "Creazione nuovo account di backup");
define('TITLE_EDIT_BACKUP_ACCOUNT', "Modifica account di backup");
define('TITLE_MODULES', "Moduli");
define('TITLE_RESELLER', "Rivenditore");
define('LABEL_BACKUP_ACCOUNT_OWNER', "Proprietario");
define('LABEL_SERVER', "Server");
define('LABEL_ID_SERVER', "Id Server");
define('LABEL_RESELLER', "Rivenditore");
define('LABEL_LOGINNAME', "Nome login");
define('LABEL_ALIAS', "Alias");
define('LABEL_CLIENTTYPE', "Client");
define('LABEL_MODMSSQL', "Microsoft SQL Server");
define('LABEL_MODMSEXCHANGESERVER', "Microsoft Exchange Server");
define('LABEL_MODORACLE', "Oracle Database Server");
define('LABEL_MODLOTUSNOTES', "Lotus Notes");
define('LABEL_MODLOTUSDOMINO', "Lotus Domino");
define('LABEL_MODMYSQL', "MySQL");
define('LABEL_MODINFILEDELTA', "In-File Delta");
define('LABEL_MODVOLUMESHADOW', "Volume Shadow Copy");
define('LABEL_MODMSEXCHANGEMBOX', "Microsoft Exchange Mailbox");
define('LABEL_MODCDP', "Continuous Data Protection");
define('LABEL_MODSHADOWPROTECT', "ShadowProtect System Backup");
define('LABEL_MODMSWINDOWSSYS', "Microsoft Windows System Backup");
define('LABEL_MODDELTAMERGE', "Delta Merge");
define('LABEL_MODMSWINVIRTUAL', "Microsoft Windows Virtualization");
define('LABEL_MODVMWARE', "VMware VM Backup");

define('LABEL_CUSTOMER', "Cliente");

define('ERROR_FIELD_CANNOT_BE_NULL', "Errore. Il seguente campo non può essere vuoto: ");
define('ERROR_REMOTE_SERVER_CALL', "Errore nella chiamata al server remoto");
define('ERROR_DATA_INSERT', "Errore durante l'inserimento dati");
define('ERROR_CONNECTION_TIMEOUT', "Timeout connessione");
define('ERROR_INVALID_USERNAME_OR_PASSWORD', "Nome utente o password non validi");
define('ERROR_NOT_LOGGED_IN', "Utente non autenticato. La sessione potrebbe essere scaduta oppure non disponi delle autorizzazioni per visualizzare questa pagina");
define('ERROR_HOST_NOT_ALLOWED', "Il server selezionato non è autorizzato");

define ('TITLE_LIST_MISSING_BACKUP_ACCOUNTS', "Elenco Account di backup mancanti");
define ('INFO_LIST_MISSING_BACKUP_ACCOUNTS', "I seguenti account di backup sono presenti sul server', ma non risultano caricati nel pannello di controllo");
define ('INFO_DOUBLE_CLICK_ROW_TO_VIEW_DETAILS', "Fare doppio clic su una riga per vedere i dettagli")       
?>