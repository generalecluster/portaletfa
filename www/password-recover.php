<?php
/*
 * Questo file è stato creato il 02-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = FALSE;
require_once 'autoload.php';
$pagetitle = TITLE_RECOVER_PASSWORD;
include 'header.php';
?>
<h1><?php echo $pagetitle?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}
if (isset($_POST['cf'])) {
    $entity = new Utente();
    $userdata = null;
    $userlist = $entity->getBy(array("codicefiscale" => $_POST['cf']));
    if (count($userlist) > 0) {
        $userdata = $userlist[0];
        $mail = $userdata->email;
        //obfuscate the email address
        $mailparts = explode('@', $mail);
        $mailname = $mailparts[0];
        $maildomain = $mailparts[1];
        $maildomainparts = explode(".",$maildomain);
        $maildomainname = $maildomainparts[0];
        $mailtld = $maildomainparts[1];        
        $mailnamelen = strlen($mailname);
        $ob_email = substr($mailname,0,1).str_repeat('*',max(array(0, $mailnamelen-2)));
        $ob_email.= substr($mailname, max(array(1,$mailnamelen-1)), 1);
        $ob_email.= "@";
        $maildomainnamelen = strlen($maildomainname);
        $ob_email.= substr($maildomainname,0,1).str_repeat('*',max(array(0, $maildomainnamelen-2)));
        $ob_email.= substr($maildomainname, max(array(1,$maildomainnamelen-1)), 1);
        $ob_email.= ".";
        $ob_email.= $mailtld;
        echo GUI::infoBox(sprintf(MESSAGE_SEND_RECOVER_PASSWORD, $ob_email));
    ?>
    <form name="frmreqpwd" id="frmreqpwd" action="do_utente.php" method="POST">
        <input type="hidden" name="action" id="action" value="reqpwd"/>
        <input type="hidden" name="cf" id="cf" value="<?php echo $_POST['cf']?>"/>
        <button type="submit"><?php echo LABEL_SEND?></button>
    </form>
    <?php
    } else {
        //fiscal code not found
        echo GUI::errorBox(ERROR_NOT_FOUND);
    }
} else {
    //this is the first step in the password request process.
    echo GUI::infoBox(MESSAGE_INSERT_SSN_NUMBER);
?>
    <form name="frmcf" id="frmcf" method="POST">
        <label for="cf" class="lbltext"><?php echo LABEL_SSN_NUMBER?></label>
        <input type="text" name="cf" id="cf"/><br/>
        <button type="submit"><?php echo LABEL_SEND?></button>
    </form>
<?php
}
include 'footer.php';
?>