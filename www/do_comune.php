<?php
/*
 * Questo file è stato creato il 11-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved=false;
require_once 'autoload.php';

$dbg = new Debug();
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);
$dbg->verbose=false;

$dbg->outln("entered do_comune");

$entity = new Comune();
if (isset($_GET['idpr'])) {
    try {
        $list = $entity->getBy(array("id_provincia" => $_GET['idpr']));
        
        echo json_encode($list);
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = ERROR_DATA_INSERT. ": ".$oEx->getMessage();
        $_SESSION['postvars'] = $_POST;
    }
} 
?>