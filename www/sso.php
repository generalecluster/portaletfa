<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

if ($_GET['action'] == "auth") {
    $reserved = TRUE;
    require_once 'autoload.php';
    //STEP 1: start an authentication process with the remote server
    $sso = new SingleSignOn("das.tfalegal.it", $user->username, $_GET['cid']);
    try {
        $sso->authenticate();
    } catch (Exception $oEx) {
        $_SESSION['errors'][] = $oEx->getMessage();
        header('Location: /error.php');
    }
} else {
    $reserved = FALSE;
    //error_log("********************************* STEP2 ************");
    require_once 'autoload.php';
    //STEP 2: handle the response from the remote server and send user credentials.
//    echo "key:".$_GET['key']."<br/>";
//    echo "appName:".$_GET['appName']; 
    $sso = new SingleSignOn("das.tfalegal.it");
    //error_log("calling checklogin");
    $retval = $sso->checkLogin($_GET['key'], $_GET['appName'], $_GET['ipforwarded'], $_GET['ipremote']);
    //error_log("called checklogin");
    echo json_encode($retval);
    //error_log("sso.php echoed ". json_encode($retval));
}

?>