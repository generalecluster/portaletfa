<!DOCTYPE html>
<?php //error_log("header included by ".$_SERVER['SCRIPT_FILENAME']);?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.6/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.6/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.js"></script>
    <?php echo $head ?>

        <title><?php echo $pagetitle ?></title>
    </head>
    <body>
        <!-- page start -->
        <div id="pagelogin">
            <!-- header start -->
            <div id="header">

                <div class="clear"></div>
            <!-- header end -->
            </div>

            <navigation><?php if (!empty($user)) { echo LABEL_WELCOME.": ".$user->nome." ".$user->cognome."&nbsp;(".$user->codicefiscale.")";?>&nbsp;<a href="../do_logout.php">Logout</a><?php } ?></navigation>

            <div class="clear"></div>
            <!-- main start -->
            <div id="main">