<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = false;
require_once 'autoload.php';
if ($_POST['action'] == "dologin") {
    $user = new Utente();
    $userlist = $user->getBy(array("username" => strtolower($_POST['username']), "password" => md5($_POST['password'])));
    $userexists = false;
    if (is_array($userlist) && !empty($userlist)) {
        if ($userlist[0]->id_utente != "") {
            if ($userlist[0]->id_stato == 4) {
                //session_start();
                $_SESSION['user'] = $userlist[0]->id_utente;
                $userexists = true;
                header('Location: index.php');
                exit();
            } else {
                $_SESSION['errors'][] = ERROR_USER_NOT_CONFIRMED;
            }
        }
    }
    if (!$userexists) {
        //session_start();
        $_SESSION['errors'][] = ERROR_INVALID_USERNAME_OR_PASSWORD;
        //echo ERROR_INVALID_USERNAME_OR_PASSWORD;
        header('Location: login.php');
    }
}
?>