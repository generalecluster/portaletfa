<?php
/*
 * Questo file è stato creato il 18-mar-2013 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = false;
$head = <<<EOT
<style>
    body{background:url('../img/login-bg.jpg') top center no-repeat;}
    #page{height: 642px;position: relative;}
    #header{background:none;}
    h1{display:none;}
    label{display:none}
    #loginwrapper{width:1024px;margin-left:auto;margin-right:auto; position:relative;}
    #frmLogin{position:absolute;top:55px; right:10px; width:308px;}
    #username{margin-bottom:50px;}
    #password{margin-bottom:10px;}
    #btnlogin{margin-left:0; margin-bottom:10px;}
    #messages{width:250px;position:absolute;right:285px; top:54px; color:red; font-size:0.9em; font-weight:bold;}
    .footercontactinfo {width:1024px; margin-left:auto; margin-right:auto; text-align:center;}
</style>
EOT;
require_once 'autoload.php';
$pagetitle = TITLE_LOGIN_PAGE;
include 'header-login.php';
?>
<div id="loginwrapper">
<h1><?php echo TITLE_LOGIN_PAGE?></h1>
<div id="messages">
<?php

if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

if (isset($_SESSION['messages'])) {
    GUI::showErrors($_SESSION['messages']);
    $_SESSION['messages'] = array();
}
?>
</div>
<form name="frmLogin" id="frmLogin" action="do_login.php" method="post">
    <ul id="listlogin">
        <li><label for="username" id="lblusername" class="lbltext"><?php echo LABEL_USERNAME."(".LABEL_SSN_NUMBER.")"?></label><input class="inputtext" type="text" id="username" name="username"/></li>
        <li><label for="password" id="lblusername" class="lbltext"><?php echo LABEL_PASSWORD?></label><input class="inputtext" type="password" id="password" name="password"/></li>
        <li>
            <input type="submit" name="btnlogin" id="btnlogin" value="<?php echo LABEL_SEND?>"/>
            <input type="hidden" name="action" id="action" value="dologin"/>
        </li>
        <li class="notregistered">Non sei registrato? <a href="register.php">Registrati</a></li>
    </ul>
</form>
</div>
<?php
include 'footer.php';
?>
