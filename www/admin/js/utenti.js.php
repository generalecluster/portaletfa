<?php require_once('../autoload.php');?>
$(document).ready(function(){
    $('#tblutenti').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "../do_utente.php",
            "data": {
                "action":"getUsersTable"
            }
        },
        columnDefs: [
            {"name": "cognome", data:"cognome", targets:0 },
            {"name": "nome", data:"nome", targets:1 },
            {"name": "codicefiscale", data:"codicefiscale", targets:2 },
            {"name": "codicerui", data:"codicerui", targets:3 },
            {"name": "cod_das", data: "cod_das", targets:4 },
            {"name": "email", data:"email", targets:5 },
            {
                "name": "id", 
                className:"linkprofile", 
                data:"id_utente", 
                render:function(data, type, row){
                    link1 = "<a href=\"userprofile.php?id="+data+"\"><img alt=\"<?php echo TITLE_PROFILE_USER?>\" title=\"<?php echo TITLE_PROFILE_USER?>\" src=\"../img/user-24.png\"/></a></td>"; 
                    return link1;
                }, 
                width:24,
                targets:6 
            },
            {
                "name": "id", 
                className:"linkprofile", 
                data:"id_utente", 
                render:function(data, type, row){
                    link1="<a href=\"utente.php?id="+data+"\"><img alt=\"<?php echo LABEL_COURSES_LIST_VIEW?>\" title=\"<?php echo LABEL_COURSES_LIST_VIEW?>\" src=\"../img/books-24.png\"/></a></td>";
                    return link1;
                }, 
                width:24,
                targets:7 
            },            
        ],
        "fnInitComplete": function() {
        this.fnAdjustColumnSizing(true);
        },
    });    
});


