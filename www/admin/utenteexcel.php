<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/** Include PHPExcel */
require_once dirname(__FILE__) . '/../classes/PHPExcel.php';
$reserved = TRUE;
set_include_path(get_include_path().":.:..");
require_once './autoload.php';

$userdata = new Utente();
$userdata->getByPrimaryKey($_GET['id']);

$iscrizione = new UtenteUnitadidattica();
$iscrizioni = $iscrizione->getBy(array("id_utente" => $_GET['id']));

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Portale DAS")
                            ->setLastModifiedBy("Portale DAS")
                            ->setTitle("Elenco iscritti al corso")
                            ->setSubject("Elenco iscritti al corso")
                            ->setDescription("Elenco iscritti al corso")
                            ->setKeywords("")
                            ->setCategory("");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Corso')
            ->setCellValue('B1', 'Area Tematica')
            ->setCellValue('C1', 'Cognome')
            ->setCellValue('D1', 'Nome')
            ->setCellValue('E1', 'Codice fiscale')
            ->setCellValue('F1', 'Codice RUI')
            ->setCellValue('G1', 'Codice DAS')
            ->setCellValue('H1', 'Crediti')
            ->setCellValue('I1', 'Completato')
            ->setCellValue('J1', 'Data superamento');

$client = LmsClientFactory::getInstance();
$cntrows = 2;
foreach ($iscrizioni as $key => $row) {
    $corso = new Unitadidattica();
    $corso->getByPrimaryKey($row->id_unitadidattica);
    $completionresult = "";
    $completiontime = $row->dt_superamento;
    $completedtasks = 0;
    $numtasks = 0;
    $completionratio = 0;
    $credits = $row->crediti;
    if (empty($row->dt_superamento)) {
        try {
            $completion = $client->getCourseCompletion($userdata, $corso->id_extern);
            $completion = json_decode($completion);
            //print_R($completion->completionstatus->completions);echo "<br/><br/>";
            if (isset($completion->completionstatus)) {
                $completionresult = $completion->completionstatus->completed;
                    //get completion date of the last activity
                foreach ($completion->completionstatus->completions as $key => $val) {
                    if (!empty($val->timecompleted)) {
                        if ($completionresult == 1) {
                            $completiontime = date("d/m/Y",$val->timecompleted);
                            $credits = $corso->crediti;
                        }
                        $completedtasks++;
                    }
                    $numtasks++;
                }
            } else {
                throw new Exception(ERROR_USER_NOT_SUBSCRIBED_COURSE, 404);
            }
            $completionratio = $completedtasks*100/$numtasks;
            
            if ($completionresult == 1) {
                //save completion data to learning unit entity
                $updlearningunit = new UtenteUnitadidattica();
                $updlearningunit->getByPrimaryKey($row->id_utente_unitadidattica);
                $updlearningunit->dt_superamento = DBConn::italianToMysqlDateTime($completiontime);
                $updlearningunit->crediti = $corso->crediti;
                $updlearningunit->id_tipoformazione = $userdata->id_tipoformazione;
                $updlearningunit->update();
            }
        } catch (Exception $oEx) {
            $completionresult = "Err:".$oEx->getMessage();
        }
    } else {
        $completionratio = 100;
        $completionresult = 1;
    }
    
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$cntrows, $corso->nome)
            ->setCellValue('B'.$cntrows, $corso->descrizione_areatematica)
            ->setCellValue('C'.$cntrows, $userdata->nome)
            ->setCellValue('D'.$cntrows, $userdata->cognome)
            ->setCellValue('E'.$cntrows, $userdata->codicefiscale)
            ->setCellValue('F'.$cntrows, $userdata->codicerui)
            ->setCellValue('G'.$cntrows, $userdata->cod_das)
            ->setCellValue('H'.$cntrows, (int)$credits)
            ->setCellValue('I'.$cntrows, $completionresult)
            ->setCellValue('J'.$cntrows, DBConn::mysqlToItalianDate($completiontime));
    $cntrows++;
}

$invalidCharacters = $objPHPExcel->getActiveSheet()->getInvalidCharacters();
$title = str_replace($invalidCharacters, '', $corso->nome);
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($corso->nome);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$corso->nome.'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
