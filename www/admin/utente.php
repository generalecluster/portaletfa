<?php
/*
 * Questo file è stato creato il 04-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
set_include_path(get_include_path().":.:..");
$head = "<script type=\"text/javascript\" src=\"js/learnunit.js\"></script>";
require_once './autoload.php';
$pagetitle = TITLE_ADMIN_PAGE;
include './header.php';
?>
<h1><?php echo TITLE_ADMIN_PAGE?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

$utenteud = new UtenteUnitadidattica();
$corsi = $utenteud->getBy(array("id_utente" => $_GET['id']));

$userdata = new Utente();
$userdata->getByPrimaryKey($_GET['id']);
?>
<h2><?php echo LABEL_COURSES_LISTOF?> - <?php echo $userdata->cognome." ".$userdata->nome?> (<?php echo $userdata->codicefiscale?>)</h2>
<table class="courselist">
    <tr><th>Corso</th><th>Area Tematica</th><th>Cognome e nome</th><th>Codice fiscale</th><th>Codice RUI</th><th>Codice DAS</th><th>Completato</th><th>Data superamento</th><th>Crediti</th><th>Attestato</th><th>Avanzamento</th></tr>
<?php
$client = LmsClientFactory::getInstance();
foreach ($corsi as $key => $row) {
    $corso = new Unitadidattica();
    $corso->getByPrimaryKey($row->id_unitadidattica);
    $completionresult = "";
    $completiontime = $row->dt_superamento;
    $completedtasks = 0;
    $numtasks = 0;
    $completionratio = 0;
    $credits = $row->crediti;
    if (empty($row->dt_superamento)) {
        try {
            $completion = $client->getCourseCompletion($userdata, $corso->id_extern);
            $completion = json_decode($completion);
            //print_R($completion->completionstatus->completions);echo "<br/><br/>";
            if (isset($completion->completionstatus)) {
                $completionresult = $completion->completionstatus->completed;
                    //get completion date of the last activity
                foreach ($completion->completionstatus->completions as $key => $val) {
                    if (!empty($val->timecompleted)) {
                        if ($completionresult == 1) {
                            $completiontime = date("d/m/Y",$val->timecompleted);
                            $credits = $corso->crediti;
                        }
                        $completedtasks++;
                    }
                    $numtasks++;
                }
            } else {
                throw new Exception(ERROR_USER_NOT_SUBSCRIBED_COURSE, 404);
            }
            $completionratio = $completedtasks*100/$numtasks;
            
            if ($completionresult == 1) {
                //save completion data to learning unit entity
                $updlearningunit = new UtenteUnitadidattica();
                $updlearningunit->getByPrimaryKey($row->id_utente_unitadidattica);
                $updlearningunit->dt_superamento = DBConn::italianToMysqlDateTime($completiontime);
                $updlearningunit->crediti = $corso->crediti;
                $updlearningunit->id_tipoformazione = $userdata->id_tipoformazione;
                $updlearningunit->update();
            }
        } catch (Exception $oEx) {
            $completionresult = "Err:".$oEx->getMessage();
        }
    } else {
        $completionratio = 100;
        $completionresult = 1;
    }
    ?>
    <tr>
        <td><?php echo $corso->nome?></td>
        <td><?php echo $corso->descrizione_areatematica?></td>
        <td><?php echo $userdata->cognome." ".$userdata->nome?></td>
        <td><?php echo $userdata->codicefiscale?></td>
        <td><?php echo $userdata->codicerui?></td>
        <td><?php echo $userdata->cod_das?></td>
        <td><?php echo $completionresult?></td>
        <td><?php echo DBConn::mysqlToItalianDate($completiontime)?></td>
        <td><?php echo (int)$credits?></td>
        <td><a href="">Attestato</a></td>
        <td><?php echo $completionratio?>%</td>
    </tr>

<?php
}
?>
</table>
<br/><br/>
<a href="utenteexcel.php?id=<?php echo $_GET['id']?>">Scarica i dati in formato Excel</a>
<?php
include './footer.php';
?>