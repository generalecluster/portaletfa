<?php
/*
 * Questo file è stato creato il 04-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"js/learnunit.js\"></script>";
require_once './autoload.php';
$pagetitle = TITLE_ADMIN_PAGE;
include './header.php';
?>
<h1><?php echo TITLE_ADMIN_PAGE?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}
$corso = new Unitadidattica();
//$corsolist = $corso->getList("id_unitadidattica", "nome", LABEL_SELEZIONARE);
$corsolist = $corso->getData();
?>
<h2><?php echo LABEL_COURSES_LISTOF?></h2>
<table class="courselist">
<?php
foreach ($corsolist->rows as $key => $row) {?>
    <tr>
        <td><a href="corso.php?id=<?php echo $row->id_unitadidattica?>"><?php echo $row->nome?></a>
        <td class="linkprofile"><a href="coursedetails.php?id=<?php echo $row->id_unitadidattica?>"><img src="../img/summary-24.png" alt="<?php echo LABEL_COURSE_DETAILS?>" title="<?php echo LABEL_COURSE_DETAILS?>"/></a></td>
        <td class="linkprofile"><a href="corsoexcel.php?id=<?php echo $row->id_unitadidattica?>"><img src="../img/download-24.png" alt="<?php echo LABEL_DOWNLOAD_REPORT_EXCEL?>" title="<?php echo LABEL_DOWNLOAD_REPORT_EXCEL?>"/></a></td>
    </tr>

<?php
}
?>
</table>

<?php
include './footer.php';
?>