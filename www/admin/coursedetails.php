<?php
/*
 * Questo file è stato creato il 14-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"\"></script>";
require_once 'autoload.php';
$pagetitle = TITLE_COURSE_DETAILS;
include 'header.php';
?>
<h1><?php echo TITLE_COURSE_DETAILS ?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

if (isset($_SESSION['messages'])) {
GUI::showMessages($_SESSION['messages']);
$_SESSION['messages'] = array();
}
?>

<?php
$entity = new Unitadidattica();
$entity->getByPrimaryKey($_GET['id']);
$entity->hideAllColumns();
$entity->showColumns(array('id_unitadidattica',
    'nome','descrizione', 'descrizione_areatematica', 
    'crediti', 'descrizione_stato','dt_ins'));
echo GUI::listFromEntity($entity, 'class="profile"');
?>
<a href="course-edit.php?id=<?php echo $_GET['id']?>"><?php echo LABEL_COURSE_EDIT?></a>
<?php
include 'footer.php';
?>