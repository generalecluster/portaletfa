<?php
/*
 * Questo file è stato creato il 04-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
set_include_path(get_include_path().":.:..");
$head = "<script type=\"text/javascript\" src=\"js/utenti.js.php\"></script>";
require_once './autoload.php';
$pagetitle = TITLE_ADMIN_PAGE;
include './header.php';
?>
<h1><?php echo TITLE_ADMIN_PAGE?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

$utente = new Utente();
$utenti = $utente->getData();
?>
<h2><?php echo LABEL_LEARNERS_LISTOF?></h2>
<table class="courselist" id="tblutenti">
    <thead>
        <tr><th>Cognome</th><th>Nome</th><th>Codice fiscale</th><th>Codice RUI</th><th>Codice DAS</th><th>E-mail</th><th class="profilelink">&nbsp;</th><th class="profilelink">&nbsp;</th></tr>
    </thead>
    <tbody>
    </tbody>
</table>
<br/><br/>
<!-a href="corsoexcel.php?id=<?php // echo $_GET['id']?>">Scarica i dati in formato Excel</a-->
<?php
include './footer.php';
?>