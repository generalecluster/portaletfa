<?php
/*
 * Questo file è stato creato il 14-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js\"></script>";
$head .= "<script type=\"text/javascript\" src=\"../js/course-edit-admin.js\"></script>";
require_once 'autoload.php';
$pagetitle = TITLE_COURSE_EDIT;
include 'header.php';
?>
<h1><?php echo TITLE_COURSE_EDIT ?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

$entity = new Unitadidattica();
$entity->getByPrimaryKey($_GET['id']);
$entity->hideAllColumns();
$entity->showColumns(array('nome','descrizione', 'id_areatematica', 
    'crediti', 'id_stato'));
$entity->setColumnHtmlType("id_areatematica", GUI::HTML_SELECT);
$entity->setColumnHtmlType("id_stato", GUI::HTML_SELECT);
?>
<form name="frmProfile" id="frmProfile" action="do_ud.php" method="post">
<?php
echo GUI::formFromEntity($entity, 'class="profile"');
?>
    <input type="hidden" name="action" id="frmaction" value="saveadm"/>
    <input type="hidden" name="id_unitadidattica" id="id_unitadidattica" value="<?php echo $entity->id_unitadidattica?>"/>
    <button type="submit" name="btnSaveProfile" id="btnSaveProfile"><?php echo LABEL_SAVE_EDITS?></button>
</form>    
<?php
include 'footer.php';
?>