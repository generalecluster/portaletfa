<?php
global $reserved;
//error_log("auth included by ".$_SERVER['PHP_SELF']);
if($reserved == TRUE) {
    if (isset($_SESSION['user'])) {
        if ($_SESSION['user'] == "") {
            //error_log("auth.php user not set");
            $_SESSION['errors']=array();
            array_push($_SESSION['errors'], ERROR_NOT_LOGGED_IN);
            header("Location: login.php");
            exit();
        } else {
            $user = new Utente();
            $user->getByPrimaryKey($_SESSION['user']);
            if ($user->id_ruolo != 1) {
                $_SESSION['errors']=array();
                array_push($_SESSION['errors'], ERROR_NOT_LOGGED_IN);
                header("Location: login.php");
                exit();
            }
    //        if (array_search($user->id_role, $roles) === false) {
    //            echo "role: ".$user->id_role."<br/>";
    //            print_r($roles);
    //            $_SESSION['errors'][] = ERROR_NOT_LOGGED_IN;
    //            //echo "not authorized";
    //            header('Location: login.php');
    //            exit();
    //        }
        }
    } else {
        $_SESSION['errors']=array();
        if ($_SERVER['REQUEST_URI'] !== "/admin/") {
            array_push($_SESSION['errors'], ERROR_NOT_LOGGED_IN);
        }
        header("Location: login.php");
        exit();
    }
}
?>
