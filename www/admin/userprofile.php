<?php
/*
 * Questo file è stato creato il 14-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"\"></script>";
require_once 'autoload.php';
$pagetitle = TITLE_PROFILE_USER;
include 'header.php';
?>
<h1><?php echo TITLE_PROFILE_USER ?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}

if (isset($_SESSION['messages'])) {
GUI::showMessages($_SESSION['messages']);
$_SESSION['messages'] = array();
}
?>

<?php
$entity = new Utente();
$entity->getByPrimaryKey($_GET['id']);
$entity->hideAllColumns();
$entity->showColumns(array('id_utente',
    'username','nome','cognome',
    'codicefiscale','email','descrizione_tipoformazione',
    'biennio','descrizione_tipoutente', 'codicerui','cod_das','descrizione_stato','dt_ins'));
echo GUI::listFromEntity($entity, 'class="profile"');

//check if user exists on the lms
try {
    $oClient = LmsClientFactory::getInstance();
    $lmsuser = $oClient->getUser($entity);
    if (empty($lmsuser)) {
?>
<div><?php echo LABEL_USER_NOT_EXISTS_LMS?></div>
<?php  
    }
} catch (Exception $oEx) {
    echo $oEx;
}
?>
<a href="userprofile-edit.php?id=<?php echo $_GET['id']?>">Modifica profilo</a>
<?php
include 'footer.php';
?>
