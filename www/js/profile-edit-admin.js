
$(document).ready(function() {
    var form = $("#frmProfile");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
            nome: {required:true},
            cognome: {required:true},
            email: {required:true, email:true},
            codicerui: {maxlength: 10, alphanum10: true},
            cod_das: {required:true, maxlength: 5, alphanum5: true},
            id_tipoutente: {required:true},
            id_stato: {required:true},
            id_tipoformazione: {required:true},
        },
        messages: {
            //password: {pwcheck: "La password deve essere lunga tra 6 e 20 caratteri, deve contenere almeno 1 lettera maiuscola, almeno 1 numero e almeno 1 carattere non alfanumerico, ad esempio *,-, oppure #. "}
            password: {pwcheck: "La password deve essere lunga tra 8 e 20 caratteri."},
            codicerui: {alphanum10: "Il codice RUI deve essere composto da 10 caratteri alfanumerici."},
            cod_das: {alphanum5: "Il codice DAS deve essere composto da 5 caratteri alfanumerici."}
        }
    });
    
    
    $.validator.addMethod("alphanum5",
                    function(value, element) {
                            var alphanumRegex = /^[a-zA-Z0-9]{5}$/;
                            retval = false;
                            //test length
                            if (alphanumRegex.test(value)) {
                                retval = true;
                            }
                            return retval;
                    });

    $.validator.addMethod("alphanum10",
                    function(value, element) {
                        if (value.length == 0) return true;
                        var alphanumRegex = /^[a-zA-Z0-9]{10}$/;
                        retval = false;
                        //test length
                        if (alphanumRegex.test(value)) {
                            retval = true;
                        }
                        return retval;
                    });

    $('#btnSaveProfile').click(function(){
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    });
    
    $('#frmProfile input').blur(function(){
         if(!$(this).valid()){
             $(this).focus();
             return false;
         }
     });
});
