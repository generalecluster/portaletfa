/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $('.biennium').hide();
    var form = $('#frmLogin');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
        //    rdlearningtype: {required: true},
        //    rdbiennium: {required:"#rdlearningadvanced:checked"}
        }
    });
    $('#btnlogin').click(function(){
        
        if (form.valid()) {
            form.submit();
        }
    });
    $('input[name=rdlearningtype]').change(function(){
        if ($(this).val() == "2") {
            $('.biennium').show();
        } else {
            $('.biennium').hide();
            $('input:radio[name=rdbiennium]:checked').prop('checked', false).checkboxradio("refresh");
        }
    });
});

