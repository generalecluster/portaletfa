/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
    $(document).ready(function(){
        $('.hidden').hide();
        $('.btnsubud').click(function(){
            $.ajax({
                dataType: "json",
                method: "POST",
                url: "do_ud.php",
                data: "ud="+$(this).attr("data-ud")+"&action=sub"
            })
            .done(function( msg ) {
              alert( "Iscrizione completata correttamente");
              //$(this).hide();
              //$(this).next('.gocourse').show();
              location.reload();
            })
            .fail(function(xhr, status, error){
                var jresp = jQuery.parseJSON(xhr.responseText);
                alert("si è verificato un errore: " + jresp.message);
            });
        });
    });
})(jQuery);