/** 
 * Drop Down Date Picker jQuery plugin
 * Copyright 2013, Alex Laudani - Softmasters
 * http://www.softmasters.net
 * 
 * ddDtPicker is a lugin for jQuery which converts a
 * hidden input field or input text field in a 
 * drop down date picker, with separate input select
 * for day, month and year.
 * The selected date is copied in the original input 
 * value when one of the selects' values changes. 
 */

(function($) {
    $.fn.dddtpicker = function() {
        $(this).hide();
        var attrName = $(this).attr('name');
        var arraysuffix = "";
        //alert(attrName.match("\[\]$"));
        if (/\[\]$/.test(attrName)) {
            attrName = attrName.slice(0, -2);
            arraysuffix = "[]";
        } 
        var sday = $('<select></select>');
        sday.attr("name", attrName+"day"+arraysuffix);
        sday.attr("id", $(this).attr("id")+"day");
        if (typeof $(this).attr("class") !== 'undefined') {
           sday.attr("class", $(this).attr("class")+" day"); 
        }
        sday.append('<option value="">gg</option>');
        for (cntday=1; cntday<=31; cntday++) {
            sday.append('<option value="' + cntday + '">' + cntday + '</option>');
        }
        sday.change(function(){
            $(this).prevAll('input').val($(this).val() + '/' + $(this).next('select').val() + '/' + $(this).siblings('select').eq(1).val());
        });
        $(this).after(sday);

        var smonth = $('<select></select>');
        smonth.attr("name", attrName+"month"+arraysuffix);
        smonth.attr("id", $(this).attr("id")+"month");
        if (typeof $(this).attr("class") !== 'undefined') {
            smonth.attr("class", $(this).attr("class")+" month");
        }
        smonth.append('<option value="">mm</option>');
        for (cntmonth=1; cntmonth<=12; cntmonth++) {
            smonth.append('<option value="' + cntmonth + '">' + cntmonth + '</option>');
        }
        smonth.change(function(){
            $(this).prevAll('input').val($(this).prev('select').val() + '/' + $(this).val() + '/' + $(this).next('select').val());
        });
        sday.after(smonth);

        var syear = $('<select></select>');
        syear.attr("name", attrName+"year"+arraysuffix);
        syear.attr("id", $(this).attr("id")+"year");
        if (typeof $(this).attr("class") !== 'undefined') {
            syear.attr("class", $(this).attr("class")+" year");
        }
        syear.append('<option value="">aaaa</option>');
        for (cntyear=1900; cntyear<=(new Date().getFullYear()-17); cntyear++) {
            syear.append('<option value="' + cntyear + '">' + cntyear + '</option>');
        }
        syear.change(function(){
            $(this).prevAll('input').val($(this).siblings('select').eq(0).val() + '/' + $(this).siblings('select').eq(1).val() + '/' + $(this).val());
        });
        smonth.after(syear);
        $(this).change(function(){
            var seldate = $(this).val().split("/");
            if (seldate.length === 3) {
                $(this).siblings('select').eq(0).val(parseInt(seldate[0],10));
                $(this).siblings('select').eq(1).val(parseInt(seldate[1],10));
                $(this).siblings('select').eq(2).val(parseInt(seldate[2],10));
            } else {
                seldate = $(this).val().split("-");
                if (seldate.length === 3) {
                    $(this).siblings('select').eq(0).val(parseInt(seldate[0],10));
                    $(this).siblings('select').eq(1).val(parseInt(seldate[1],10));
                    $(this).siblings('select').eq(2).val(parseInt(seldate[2],10));
                }
            }
        });
        return this;
    }
})(jQuery);


