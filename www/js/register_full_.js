/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $("#dt_nascita").dddtpicker();
    var checkcf = function(cf, sesso, giorno, mese, anno) {
        //Controllo CF
        var cf, i, s, set1, set2, setpari, setdisp;
        var Giorno;
        var fcf=/^[A-Z0-9]{16}$/;
        cf=trim(cf.toUpperCase());
        if( cf == "" ){return false;}
        if( !fcf.test(cf) ) {return false;}

        set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
        s = 0;
        for( i = 1; i <= 13; i += 2 ){s += setpari.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));}
        for( i = 0; i <= 14; i += 2 ){s += setdisp.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));}
        if( s%26 != cf.charCodeAt(15)-'A'.charCodeAt(0) ){return false;}

        if (cf.substr(6,2) != objform.Anno.value.substr(2,2)){return false;}

        switch(mese) {

        case "01": Mese = "A";break;
        case "02": Mese = "B";break;
        case "03": Mese = "C";break;
        case "04": Mese = "D";break;
        case "05": Mese = "E";break;
        case "06": Mese = "H";break;
        case "07": Mese = "L";break;
        case "08": Mese = "M";break;
        case "09": Mese = "P";break;
        case "10": Mese = "R";break;
        case "11": Mese = "S";break;
        case "12": Mese = "T";break;}

        if (cf.substr(8,1) != Mese ){return false;}
        if (sesso == "M") {Giorno = giorno;}

        else if (sesso == "F")
        {
        //controllo perchè non funziona il parseInt su un numero che inizia con 0.
        if (parseInt(giorno) == 0)
        {giorno = parseInt(giorno.substr(1,1)) + 40;}
        else {giorno = parseInt(giorno) + 40;}
        }
        if (cf.substr(9,2) != giorno ) {return false;}

        return true;

    }
    

    var form = $("#frmRegister");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            nome: {required:true},
            cognome: {required:true},
            codicefiscale: {
                required:true,
                remote: {
                    url: "do_utente.php",
                    type: "post",
                    data: {
                        action:"cf",
                        cf: function(){return $('#codicefiscale').val()},
//                        gn: function(){return $('#dt_nascitaday').val()},
//                        mn: function(){return $('#dt_nascitamonth').val()},
//                        an: function(){return $('#dt_nascitayear').val()},
//                        sex: function(){return $("input[name='id_sex']:checked").val()},
//                        com: function(){return $('#id_comune_nascita').val()}
                    }
                }
            },
            email: {required:true, email:true},
            password: {required:true, pwcheck:true},
            password2: {equalTo: "#password"},
            id_tipoformazione: {required:true},
            rdhaverui: {required: "#rdformazionedone:checked"},
            codicerui: {required: "#rdhaveruiyes:checked"},
            id_tipoutente: {required:true},
            id_tipoutente2: {required:"#rdformazionedoing:checked"},
            cod_agenzia: {required:true},
            cod_das: {required:true}
    //        confirm: {
    //            equalTo: "#password"
    //        }
        },
        messages: {
            password: {pwcheck: "La password deve essere lunga tra 6 e 20 caratteri, deve contenere almeno 1 lettera maiuscola, almeno 1 numero e almeno 1 carattere non alfanumerico, ad esempio *,-, oppure #. "}
        }
    });
    
//    $.validator.addMethod("cfcheck",
//                        function(value, element) {
//                            return checkcf(value, $('#dt_nascitaday').val(), value, $('#dt_nascitamonth').val(), value, $('#dt_nascitayear').val(), $("input[name='id_sex']:checked").val(), $('#id_comune_nascita').val());
//                            
//                        });
    
    $.validator.addMethod("pwcheck",
                        function(value, element) {
                            retval = false;
                            //test containing at least 1 non alphanumeric character
                            if (/[^a-zA-Z0-9]+/.test(value)) {
                                if (value.length >=6 && value.length<=20) {
                                    if (/[A-Z]+/.test(value)) {
                                        if (/[0-9]+/.test(value)) {
                                            retval = true;
                                        }
                                    }
                                }
                            }
                            return retval;
                    });
    
    $("#wizard").steps({
        headerTag: "h1",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (newIndex > currentIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            } else {
                //skip validation on previous button
                return true;
            }
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            //alert("priorindex " + priorIndex);
            console.log("priorindex: " + priorIndex + ", currentindex: " + currentIndex);
            if (currentIndex > priorIndex) {
                //going forward
                if (priorIndex === 1) {
                    //check value of selected radiobutton
                    var selectedbtn = $('input[name=id_tipoformazione]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "1") {
                        //alert("jump to 5");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 5);
                    }
                }
                if (priorIndex === 2) {
                    //check value of selected radiobutton
                    var selectedbtn = $('input[name=rdhaverui]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "0") {
                        //alert("jump to 4");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 4);
                    }
                }
    //            if (priorIndex === 3) {
    //                $("#wizard").steps("setStep", 6);
    //            }
                if (priorIndex === 4) {
                    $("#wizard").steps("setStep", 6);
                }
            }
            if (currentIndex < priorIndex) {
                //going backward            
                if (priorIndex === 1) {
                    //check value of selected radiobutton
                    var selectedbtn = $('input[name=id_tipoformazione]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "1") {
                        //alert("jump to 5");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 5);
                    }
                }
                if (priorIndex === 2) {
                    //check value of selected radiobutton
                    var selectedbtn = $('input[name=rdhaverui]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "0") {
                        //alert("jump to 4");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 4);
                    }
                }
                if (priorIndex === 4) {
                    var selectedbtn = $('input[name=rdhaverui]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "0") {
                        //alert("jump to 4");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 2);
                    }
                }
                if (priorIndex === 6) {
                    var selectedbtn = $('input[name=id_tipoformazione]:checked').val();
                    //alert("selected " + selectedbtn);
                    if (selectedbtn === "1") {
                        //alert("jump to 4");
                        //skip asking rui code and jump to the end
                        $("#wizard").steps("setStep", 5);
                    } else if (selectedbtn === "2") {
                        $("#wizard").steps("setStep", 4);
                    }
                }
                if (priorIndex === 5) {
                    $("#wizard").steps("setStep", 1);
                }
            }            
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });
    
    $('#id_prov_nascita').change(function(){
        $.ajax({
            dataType: "json",
            method: "GET",
            url: "do_comune.php",
            data: "idpr="+$(this).val()
        })
        .done(function( msg ) {
            $('#id_comune_nascita option:gt(0)').remove();
            var $sel = $('#id_comune_nascita');
            $.each(msg.rows, function(key, value) {
                $sel.append($('<option></option>').attr("value", value.cell[0]).text(value.cell[1]));
            }); 
        })
        .fail(function(xhr, status, error){
            var jresp = jQuery.parseJSON(xhr.responseText);
            alert("si è verificato un errore: " + jresp.message);
        });
    });
});


