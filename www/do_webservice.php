<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'autoload.php';
require_once 'classes/phpMailer/PHPMailerAutoload.php';

$dbg = new Debug();
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);
$dbg->verbose=false;
//accept only json
$input = file_get_contents('php://input');
error_log("Data: ".$input);
$postdata = json_decode($input);
error_log("Postdata: ".print_r($postdata,1));
if (isset($postdata->action) && $postdata->action == "addvmuser") {
    //save user from virtuemart registration
    require_once(__DIR__.'/classes/log4php/Logger.php');
        
    // Tell log4php to use our configuration file.
    Logger::configure(__DIR__.'/config.xml');
    
    // Fetch a logger, it will inherit settings from the root logger
    $log = Logger::getLogger('WsPlugin');
    $log->debug("Saving data: ".$_POST);
    $response = new stdClass();
    try {
        $ut = new UtenteVm("","");
        $ut->email = $postdata->email;
        $ut->username = $postdata->username;
        $ut->password = md5($postdata->password);
        $ut->nome = $postdata->first_name;
        $ut->cognome = $postdata->last_name;
        $ut->codicefiscale = $postdata->codice_fiscale;
        $ut->id_stato = 4;
        $ut->id_external = $postdata->virtuemart_user_id;
        $ut->id_customer = $postdata->customer_number;
        $ut->tos = $postdata->tos;
        $ut->piva = $postdata->partita_iva;
        $newid = $ut->insert();
        $log->debug("User saved. Id:".$newid);
        $response->code = 200;
        $response->id = $newid;
        $response->message = 'User created.';
        echo json_encode($response);
    } catch (Exception $ex) {
        $log->fatal("Error saving user. \r\n".print_r($ex,1));
        $response->code = $ex->getCode();
        $response->id = -1;
        $response->message = $ex->getMessage();
        echo json_encode($response);
    }
}
