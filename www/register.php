<?php
/*
 * Questo file è stato creato il 05-11-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = FALSE;
require_once 'autoload.php';
$pagetitle = TITLE_REGISTER_PAGE;

$head = "<script type=\"text/javascript\" src=\"js/jquery.steps.js\"></script>";
$head .= "<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js\"></script>";
$head .= "<link type=\"text/css\" href=\"css/jquery.steps.css\" rel=\"stylesheet\">";
$head .= "<script type=\"text/javascript\" src=\"js/dddtpicker.js\"></script>";
$head .= "<script type=\"text/javascript\" src=\"js/register.js\"></script>";

include 'header.php';
?>
<h1><?php echo $pagetitle?></h1>
<?php
if (isset($_SESSION['postvars'])) {
    $form = @$_SESSION['postvars'];
    unset($_SESSION['postvars']);
}

$tipoutente = new BasicEntity("tipoutente");
//$provincia = new Provincia();
//$provincia->sqlorderby = "ORDER BY descrizione_provincia"; 
//$provlist = $provincia->getList("id_provincia", "descrizione_provincia", LABEL_SELEZIONARE);
$provlist="";
//$comune = new Comune();
//$comlist = $comune->getList("id_comune", "nome", LABEL_SELEZIONARE);
$comlist = "";
?>
<form name="frmRegister" id="frmRegister" action="do_utente.php" method="post">
    <div id="wizard">
        <h1>Dati personali</h1>
        <div>
            <?php
            if (isset($_SESSION['errors'])) {
                GUI::showErrors($_SESSION['errors']);
                foreach ($_SESSION['errors'] as $key => $value) {
                    //look if there is a primary key violation on user.
                    //In that case, show a "Recover password" link.
                    if (strstr($value, 'SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry') && strstr($value, "codicefiscale_UNIQUE")) {
                        echo GUI::errorBox(ERROR_SSN_ALREADY_EXISTS.' - <a href="password-recover.php">'.LABEL_RECOVER_PASSWORD."</a>");
                    }
                }
                $_SESSION['errors'] = array();
            }
            ?>
            <ul class="wizlist">
                <li><label for="nome" id="lblnome" class="lbltext required"><?php echo LABEL_NAME?></label><input class="inputtext" type="text" id="nome" name="nome"/></li>
                <li><label for="cognome" id="lblcognome" class="lbltext required"><?php echo LABEL_SURNAME?></label><input class="inputtext" type="text" id="cognome" name="cognome"/></li>
                <!--li><label for="dt_nascita" id="lbldtnascita" class="lbltext required"><?php echo LABEL_DT_BIRTH?></label><input class="inputtext" type="text" id="dt_nascita" name="dt_nascita"/></li>
                <li><label for="id_prov_nascita" id="lblprovnascita" class="lbltext required"><?php echo LABEL_PROVINCE_OF_BIRTH?></label><select class="inputtext" name="id_prov_nascita" id="id_prov_nascita"><?php echo $provlist?></select></li>
                <li><label for="id_comune_nascita" id="lblcomunenascita" class="lbltext required"><?php echo LABEL_CITY_OF_BIRTH?></label><select class="inputtext" name="id_comune_nascita" id="id_comune_nascita"><?php echo $provlist?></select></li-->
                <li><label for="codicefiscale" id="lblcodicefiscale" class="lbltext required"><?php echo LABEL_SSN_NUMBER?></label><input class="inputtext" type="text" id="codicefiscale" name="codicefiscale"/></li>
                <li><label for="email" id="lblemail" class="lbltext required"><?php echo LABEL_EMAIL?></label><input class="inputtext" type="text" id="email" name="email"/></li>
                <li><label for="email" id="lblemail2" class="lbltext required"><?php echo LABEL_EMAIL_CONFIRM?></label><input class="inputtext" type="text" id="email2" name="email2"/></li>
                <li><label for="password" id="lblpassword" class="lbltext required"><?php echo LABEL_PASSWORD?></label><input class="inputtext" type="password" id="password" name="password"/></li>
                <li><label for="password2" id="lblpassword2" class="lbltext required"><?php echo LABEL_PASSWORD_CONFIRM?></label><input class="inputtext" type="password" id="password2" name="password2"/></li>
                <!--li>
                    <input type="submit" name="btnsubmit" id="btnsubmit" value="<?php echo LABEL_SEND?>"/>
                    <input type="hidden" name="action" id="action" value="dologin"/>
                </li-->
            </ul>            
        </div>
     
        <!--h1>Tipo Formazione</h1>
        <div>
            <h2><?php echo LABEL_CHOOSE_OPTION?></h2>
            <fieldset id='fld60hrs' >
                <legend><?php echo LABEL_CHOOSE_OPTION?></legend>
                <ul class="wizlist">
                    <li>
                        <label for="rdformazionedoing" class="lbltext"><?php echo LABEL_60HRS_DOING?></label>
                        <input type='radio' name="id_tipoformazione" id="rdformazionedoing" value="1" />
                    </li>
                    <li>
                        <label for="rdformazionedone" class="lbltext"><?php echo LABEL_60HRS_DONE?></label>
                        <input type='radio' name="id_tipoformazione" id="rdformazionedone" value="2" />
                    </li>
                </ul>
            </fieldset>
        </div-->
        <h1><?php echo LABEL_BIENNIUM?></h1>
        <div>
            <div><?php echo LABEL_SELECT_BIENNIUM?></div>
            <ul class="wizlist">
                <li>
                    <label for="rdbiennium1" class="lbltext"><?php echo (date('Y')-1)."/".date('Y')?></label>
                    <input type='radio' name="biennio" id="rdbiennium1" value="<?php echo date("Y")-1?>" />
                </li>
                <li>
                    <label for="rdbiennium2" class="lbltext"><?php echo date('Y')."/".(date('Y')+1)?></label>
                    <input type='radio' name="biennio" id="rdbiennium2" value="<?php echo date("Y")?>" />
                </li>
            </ul>
        </div>
        
        <h1>Codice RUI</h1>
        <div>
            <h2><?php echo LABEL_ASK_HAVE_RUI_CODE?></h2>
            <fieldset id='fld60hrs' >
                <legend><?php echo LABEL_CHOOSE_OPTION?></legend>
                <ul class="wizlist">
                    <li>
                        <label for="rdhaveruiyes" class="lbltext"><?php echo LABEL_YES?></label>
                        <input type='radio' name="rdhaverui" id="rdhaveruiyes" value="1" />
                    </li>
                    <li>
                        <label for="rdhaveruino" class="lbltext"><?php echo LABEL_NO?></label>
                        <input type='radio' name="rdhaverui" id="rdhaveruino" value="0" />
                    </li>
                </ul>
            </fieldset>
        </div>
        
        <h1><?php echo LABEL_INSERT_RUI_CODE?></h1>
        <div>
            <label for="codicerui" id="lblcodicerui" class="lbltext required"><?php echo LABEL_RUI_CODE?></label><input class="inputtext" type="text" id="codicerui" name="codicerui"/>
        </div>
        
        <h1><?php echo LABEL_ROLE_SELECT?></h1>
        <div>
            <label for="id_tipoutente" id="lblcodicerui" class="lbltext required"><?php echo LABEL_SELECT_YOUR_ROLE?></label>
            <select name="id_tipoutente" id="id_tipoutente">
                <?php echo $tipoutente->getList("id_tipoutente","descrizione_tipoutente", LABEL_SELEZIONARE);?>
            </select>
        </div>
        
        <!--h1><?php echo LABEL_SELECT_YOUR_WANNABE_ROLE?></h1>
        <div>
            <label for="id_tipoutente2" id="lblcodicerui" class="lbltext required"><?php echo LABEL_SELECT_YOUR_WANNABE_ROLE?></label>
            <select name="id_tipoutente2" id="id_tipoutente2">
                <?php echo $tipoutente->getList("id_tipoutente","descrizione_tipoutente", LABEL_SELEZIONARE);?>
            </select>
        </div-->
        
        <h1><?php echo LABEL_AGENCY_CODE?></h1>
        <div>
            <ul class="wizlist">
                <li>
                    <label for="cod_das" id="lblcod_das" class="lbltext required"><?php echo LABEL_DAS_CODE?></label><input class="inputtext" type="text" id="cod_das" name="cod_das"/>
                    <input type="hidden" name="action" id="action" value="add"/>
                </li>
                <li>
                    <label for="fl_privacy" id="lblfl_privacy" class="lbltext required"><?php echo LABEL_PRIVACY?></label><input class="inputtext" type="checkbox" id="fl_privacy" name="fl_privacy" value="1"/>
                </li>
            </ul>
        </div>
    </div>
    
</form>
<script type="text/javascript">
$(document).ready(function() {
//generates javascript to populate form fields with submitted values.
<?php if (isset($form)) echo GUI::populateFormJS($form, new Utente());?>
});
</script>
<?php
include 'footer.php';
?>
