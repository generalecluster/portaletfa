<?php
/* 
 * Questo file è stato creato il 3-mag-2010 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a Vittoria Assicurazioni.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

class GUI {
    const HTML_SELECT = 1;
    const BIENNIUM = 2;
    const FILE_UPLOAD = 3;
    
    public static function errorBox($text) {
        $out = "<div class=\"ui-widget\">".
               " <div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">".
               "  <p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>".
               $text.
               "  </p>".
               " </div>".
               "</div>";
        return $out;
    }

    public static function infoBox($text) {
        $out = "<div class=\"ui-widget\">".
               " <div class=\"ui-state-highlight ui-corner-all\" style=\"padding: 0 .7em;\">".
               "  <p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>".
               $text.
               "  </p>".
               " </div>".
               "</div>";
        return $out;
    }

    public static function populateFormJS($aForm, $oEntity) {
        $sResult = "";
        
        foreach($aForm as $key => $val) {
            if (!is_array($val)) {
                $sResult.='$("#'.$key.'").val('.json_encode($val).');'.PHP_EOL;
                if ($oEntity!=NULL) {
                    if ($oEntity->options['colModel'][$key]->sorttype == 'date' || $oEntity->options['colModel'][$key]->sorttype == 'datetime') {
                        //$sResult.='$("#'.$key.'").datepicker({dateFormat: "dd-mm-yy"});';
                        $sResult.='$("#'.$key.'").val('.json_encode($val).');'.PHP_EOL;
                    } else if ($oEntity->options['colModel'][$key]->sorttype == 'boolean') {
                        $sResult.='$("#'.$key.'").attr("checked",'.json_encode($val).');'.PHP_EOL;
                        $sResult.='$("#'.$key.'").val(1);'.PHP_EOL;
                    }
                }
            }
        }
        return $sResult;
    }

    public static function showErrors($aErrors) {
        if (isset($aErrors))
            $errors = $aErrors;
        else
            $errors = array();

        foreach ($errors as $msg) {
            echo GUI::errorBox($msg);
        }
    }

    public static function showMessages($aMessages) {
        if (isset($aMessages))
            $errors = $aMessages;
        else
            $errors = array();

        foreach ($errors as $msg) {
            echo GUI::infoBox($msg);
        }
    }
    
    public static function populateMultirow($multirowname, $variables) {
        $result = "";
        //print_r($variables);
        if (is_array($variables) && !empty($variables)){
            if (is_array($variables[0]) && !empty($variables[0])) {
                $currindex = 0;
                foreach ($variables[0] as $key=>$value) {
                    $result.= "$multirowname.addRow([";
                    foreach ($variables as $arrkey=>$arrvalue) {
                        $result.= json_encode($arrvalue[$currindex]).',';
                    }
                    $result = substr($result, 0, -1);
                    $result.="]);\r\n";
                    $currindex++;
                }
            }
        }
        return $result;
    }
    
    /**
     * Generate a <dl> definition list to display entity data on a html page
     * @param type $entity the entity to take the data from
     * @param type $attrs html attributes to be added to the dl element
     * @return string the html of the dl list containing the entity data
     */
    public static function listFromEntity($entity, $attrs) {
        $sResult = "<dl ".$attrs.">";
        
        foreach($entity->options['colModel'] as $col_id => $column) {
            if ($column->hidden != TRUE) {
                $sResult.="<dd>".$column->title."</dd>\r\n";
                $sResult.="<dt>".(empty($entity->$col_id)? "&nbsp;" : $entity->$col_id)."</dt>\r\n";
            }
        }
        $sResult.="</dl>";
        return $sResult;
    }
    
    /**
     * Generate a unordered list with form fields from the specified entity
     * @param type $entity the entity to take the data from
     * @param type $attrs html attributes to be added to the list
     * @return string the html of the list containing the fields of the entity
     */
    public static function formFromEntity($entity, $attrs) {
        $sResult = "<ul ".$attrs.">";
        foreach($entity->options['colModel'] as $col_id => $column) {
            if ($column->hidden != TRUE) {
                $sResult.="<li>";
                //$sResult.="<br>colname:".$column->name." - htmltype: ".$column->htmltype;
                $sResult.="<label for=\"$col_id\">".$column->title."</label>\r\n";
                if (isset($column->htmltype) && $column->htmltype == GUI::HTML_SELECT) {
                    $sResult.="<select name=\"".$col_id."\" id=\"".$col_id."\">\r\n";
                    /* find the referenced table from the key name.
                     * It is expected that all foreign key are in the form: id_<classname>
                     * where <classname> is the name of the entity class to be instantiated.
                    */
                    if (strpos($col_id, "id_") != 0) {
                        throw new Exception(ERROR_BAD_KEY_FORMAT." : ".$col_id);
                    }
                    $classname = ucfirst(mb_substr($col_id, 3));
                    $selEntity = new $classname();
                    $sResult.= $selEntity->getList($col_id, DbBaseObject::$PREFIX_DESCRIPTION.$selEntity->dbtablename,$entity->$col_id, LABEL_SELEZIONARE);
                    $sResult.="</select>";
                } else if (isset($column->htmltype) && $column->htmltype == GUI::BIENNIUM) {
                    $sResult .=  '<ul class="editbiennium">
                        <li>
                            <label for="rdbiennium1" class="lbltext">'.(date('Y')-1)."/".date('Y').'</label>
                            <input '.($entity->biennio==(date("Y")-1)?"checked=\"checked\"":"").' type="radio" name="biennio" id="rdbiennium1" value="'.(date("Y")-1).'" />
                        </li>
                        <li>
                            <label for="rdbiennium2" class="lbltext">'.date('Y')."/".(date('Y')+1).'</label>
                            <input '.($entity->biennio==date("Y")?"checked=\"checked\"":"").' type="radio" name="biennio" id="rdbiennium2" value="'.date("Y").'" />
                        </li></ul>';
                } else if (isset($column->htmltype) && $column->htmltype == GUI::FILE_UPLOAD) {
                    if (!empty($entity->$col_id)) {
                        //show currently uploaded file
                        $sResult.="<span class=\"uploadedfile\">".LABEL_CURRENTLY_UPLOADED_FILE." ".$entity->$col_id."</span>";
                    }
                    $sResult.="<input name=\"".$col_id."\" id=\"".$col_id."\" type=\"file\" />";
                } else {
                    $sResult.="<input name=\"".$col_id."\" id=\"".$col_id."\" type=\"text\" value=\"".$entity->$col_id."\" />\r\n";
                }
                $sResult.="</li>";
            }
        }
        $sResult.="</ul>";
        return $sResult;
    }
}
?>
