<?php
/* 
 * Questo file è stato creato il 3-mag-2010 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a Vittoria Assicurazioni.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
//require_once('conf/config.php');

class DBConn {
    public static $mysqlDateFormat = "Y-m-d H:i:s";
    
    public static function getConn() {
        //global $CONFIG_db_connstring,$CONFIG_db_user,$CONFIG_db_pass;
        //echo "connstring: ".$CONFIG_db_connstring;
        $config = new Config();
        $dbh = new PDO($config->dbConnstring, $config->dbUser, $config->dbPass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->exec("SET CHARACTER SET utf8");
        return $dbh;
    }
    
    public static function italianToMysqlDate($dt) {
        $dt = str_replace("/","-",$dt);
        if ($dt!="" || $dt!=null) {
            $newdt = new DateTime($dt);
            return $newdt->format('Y-m-d');
        } else {
            return null;
        }
    }

    public static function mysqlToItalianDate($dt) {
        if ($dt!="" || $dt!=null) {
            $newdt = new DateTime($dt);
            return $newdt->format('d-m-Y');
        }
    }

    public static function italianToMysqlDateTime($dt) {
        $dt = str_replace("/","-",$dt);
        if ($dt!="" || $dt!=null) {
            $newdt = new DateTime($dt);
            return $newdt->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public static function mysqlToItalianDateTime($dt) {
        if ($dt!="" || $dt!=null) {
            $newdt = new DateTime($dt);
            return $newdt->format('d-m-Y H:i:s');
        }
    }
    /**
     * Calculates a date lying a given number of months in the future of a given date.
     * The results resemble the logic used in MySQL where '2009-01-31 +1 month' is '2009-02-28' rather than '2009-03-03' (like in PHP's strtotime).
     *
     * @author alex
     * @since 2009-02-03
     * @param $base_time long, The timestamp used to calculate the returned value .
     * @param $months int, The number of months to jump to the future of the given $base_time.
     * @return long, The timestamp of the day $months months in the future of $base_time
     */
    public static function get_x_months_to_the_future( $base_time = null, $months = 1 )
    {
        if (is_null($base_time))
            $base_time = time();

        $x_months_to_the_future    = strtotime( "+" . $months . " months", $base_time );

        $month_before              = (int) date( "m", $base_time ) + 12 * (int) date( "Y", $base_time );
        $month_after               = (int) date( "m", $x_months_to_the_future ) + 12 * (int) date( "Y", $x_months_to_the_future );

        if ($month_after > $months + $month_before)
            $x_months_to_the_future = strtotime( date("Ym01His", $x_months_to_the_future) . " -1 day" );

        return $x_months_to_the_future;
    } //get_x_months_to_the_future()

}
?>
