<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
include 'autoload.php';

class Unitadidattica extends DbBaseObject {
    
    public function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id, $pager_id);
        $this->dbtablename = 'unitadidattica';
        $this->addColumn("id_".$this->dbtablename, "ID", "50", "int", "true", false);
        $this->addColumn("nome", TITLE_NAME, "200", "string", "true", true);
        $this->addColumn("descrizione", TITLE_DESCRIPTION, "200", "string", "true", true);
        $this->addColumn("crediti", TITLE_CREDITS, "50", "int", "true", true);
        $this->addColumn("id_extern", TITLE_ID_EXTERN, "50", "int", "true", true);
        $this->addColumn("id_areatematica", TITLE_THEMATIC_AREA_ID, "50", "int", "true", true);
        $this->addColumn("descrizione_areatematica", TITLE_THEMATIC_AREA, "200", "string", "true", false);
        $this->addColumn("id_stato", TITLE_STATUS, "50", "int", "true", true);
        $this->addColumn("descrizione_stato", TITLE_STATUS, "200", "string", "true", false);
        $this->addColumn("dt_ins", TITLE_DT_INS, "200", "datetime", "true", false);
        $this->addColumn("dt_mod", TITLE_DT_MOD, "200", "datetime", "true", true);

        $this->sqlselect = "SELECT a.*, b.descrizione_areatematica, c.descrizione_stato ";
        $this->sqlfrom = "FROM $this->dbtablename a LEFT JOIN areatematica b".
                " ON a.id_areatematica = b.id_areatematica".
                " LEFT JOIN stato c ON a.id_stato=c.id_stato";                
    }
    
}

