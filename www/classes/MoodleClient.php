<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MoodleClient implements LmsClient {
    private function callFunction($funcName, $params) {
        $config = new Config();
        
        $serverurl = $config->lmsurl . '/webservice/rest/server.php'. '?wstoken=' . $config->lmsapitoken . '&wsfunction='.$funcName.'&moodlewsrestformat=json';
        require_once(__DIR__.'/curl.php');
        $curl = new curl;
        //if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
        //$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
        //print_r($params);
        $resp = $curl->post($serverurl, $params);
        return $resp;
    }
    
    public function createUser(Utente $oUser) {
        $user1 = new stdClass();
        $user1->username = $oUser->username;
        $user1->password = $oUser->password;
        $user1->firstname = $oUser->nome;
        $user1->lastname = $oUser->cognome;
        $user1->email = $oUser->email;
        //error_log("createUser moodle: ". $oUser->descrizione_tipoutente);
        $user1->customfields = array(
            array('type' => 'codicerui', 'value' => $oUser->codicerui),
            array('type' => 'codicedas', 'value' => $oUser->cod_das),
            array('type' => 'attivita', 'value' => $oUser->descrizione_tipoutente)
        );
        /*
        $user1->auth = 'manual';
        $user1->idnumber = 'testidnumber1';
        $user1->lang = 'en';
        $user1->theme = 'standard';
        $user1->timezone = '-12.5';
        $user1->mailformat = 0;
        $user1->description = 'Hello World!';
        $user1->city = 'testcity1';
        $user1->country = 'au';
        */
        $users[] = $user1;
        $params = array('users' => $users); 
        $result = $this->callFunction('core_user_create_users', $params);
        $result = json_decode($result);
        if (isset($result->exception)) {
            error_log("MOODLE ERROR:".$result->message." - ".$result->debuginfo);
            throw new Exception($result->message." - ".$result->debuginfo);
        }
        return $result;
    }  
    
    public function updateUser(Utente $oUser, $createifnotexist=false) {
        $params = array('field' => 'username', 'values' => array($oUser->username));
        $mdlusers = $this->callFunction("core_user_get_users_by_field", $params);
        $mdlusers = json_decode($mdlusers);
        if (isset($mdlusers->exception)) {
            error_log("MOODLE ERROR:".$mdlusers->message." - ".$mdlusers->debuginfo);
            throw new Exception($mdlusers->message." - ".$mdlusers->debuginfo);
        } else {
            if (count($mdlusers) > 0) {
                $user1 = new stdClass();
                $user1->id = $mdlusers[0]->id;
                $user1->username = $oUser->username;
                $user1->firstname = $oUser->nome;
                $user1->lastname = $oUser->cognome;
                $user1->email = $oUser->email;
                //error_log("createUser moodle: ". $oUser->descrizione_tipoutente);
                $user1->customfields = array(
                    array('type' => 'codicerui', 'value' => $oUser->codicerui),
                    array('type' => 'codicedas', 'value' => $oUser->cod_das),
                    array('type' => 'attivita', 'value' => $oUser->descrizione_tipoutente),
                    array('type' => 'learningtype', 'value' => $oUser->descrizione_tipoformazione)
                );
                /*
                $user1->auth = 'manual';
                $user1->idnumber = 'testidnumber1';
                $user1->lang = 'en';
                $user1->theme = 'standard';
                $user1->timezone = '-12.5';
                $user1->mailformat = 0;
                $user1->description = 'Hello World!';
                $user1->city = 'testcity1';
                $user1->country = 'au';
                */
                $users[] = $user1;
                $params = array('users' => $users); 
                $result = $this->callFunction('core_user_update_users', $params);
                return $result;
            } else {
                if ($createifnotexist == true) {
                    $this->createUser($oUser);
                } else {
                    throw new Exception(ERROR_NOT_FOUND, 404);
                }
            }
        }
    }
    
    public function selfEnrolUser(Utente $oUser, $courseid) {
        //get user data from username
        //error_log("self enrol user. user: ".$oUser->username." - course:".$courseid);
        $params = array('field' => 'username', 'values' => array($oUser->username));
        $mdlusers = $this->callFunction("core_user_get_users_by_field", $params);
        $mdlusers = json_decode($mdlusers);
        if (isset($mdlusers->exception)) {
            error_log("MOODLE ERROR:".$mdlusers->message." - ".$mdlusers->debuginfo);
            throw new Exception($mdlusers->message." - ".$mdlusers->debuginfo);
        } else {
            if (count($mdlusers) > 0) {
                //enrol user to course
                //error_log("self enrole user . user found. Enroling user ".$mdlusers[0]->id." - course: ".$courseid);
                $enrolment1 = new stdClass();
                $enrolment1->roleid = 5; //student
                $enrolment1->userid = $mdlusers[0]->id;
                $enrolment1->courseid = $courseid;
                $enrolments[] = $enrolment1;
                $params = array('enrolments' => $enrolments);
                $resp =  $this->callFunction("enrol_manual_enrol_users", $params);
                //error_log("self enrol user. resp: ".$resp);
                return $resp;
            } else {
                throw new Exception($message, 404);
            }
        }
    }
    
    public function getCourseCompletion(Utente $oUser, $courseid) {
        $params = array('field' => 'username', 'values' => array($oUser->username));
        $mdlusers = $this->callFunction("core_user_get_users_by_field", $params);
        $mdlusers = json_decode($mdlusers);
        if (isset($mdlusers->exception)) {
            error_log("MOODLE ERROR:".$mdlusers->message." - ".$mdlusers->debuginfo);
            throw new Exception($mdlusers->message." - ".$mdlusers->debuginfo);
        } else {
            if (count($mdlusers) > 0) {
                //enrol user to course
                //error_log("self enrole user . user found. Enroling user ".$mdlusers[0]->id." - course: ".$courseid);
                $enrolment1 = new stdClass();
                $enrolment1->roleid = 5; //student
                $enrolment1->userid = $mdlusers[0]->id;
                $enrolment1->courseid = $courseid;
                $enrolments[] = $enrolment1;
                $params = array('userid' => $mdlusers[0]->id, 'courseid' => $courseid);
                $resp =  $this->callFunction("core_completion_get_course_completion_status", $params);
                //error_log("self enrol user. resp: ".$resp);
                return $resp;
            } else {
                throw new Exception($message, 404);
            }
        }
    }
    
    public function getUser(Utente $oUser) {
        $params = array('field' => 'username', 'values' => array($oUser->username));
        $mdlusers = $this->callFunction("core_user_get_users_by_field", $params);
        $mdlusers = json_decode($mdlusers);
        if (isset($mdlusers->exception)) {
            error_log("MOODLE ERROR:".$mdlusers->message." - ".$mdlusers->debuginfo);
            throw new Exception($mdlusers->message." - ".$mdlusers->debuginfo);
        } else {
            if (count($mdlusers) > 0) {
                return $mdlusers[0];
            } else {
                return null;
            }
        }
    }
}

