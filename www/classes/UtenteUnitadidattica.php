<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
include 'autoload.php';

class UtenteUnitadidattica extends DbBaseObject {
    
    public function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id, $pager_id);
        $this->dbtablename = 'utente_unitadidattica';
        $this->addColumn("id_".$this->dbtablename, "ID", "50", "int", "true", false);
        $this->addColumn("id_utente", TITLE_ID_USER, "50", "int", "true", true);
        $this->addColumn("id_unitadidattica", TITLE_LEARNING_UNIT_ID, "50", "int", "true", true);
        $this->addColumn("id_tipoformazione", "id_tipoformazione", "50", "int", "true", true);
        $this->addColumn("crediti", TITLE_CREDITS, "50", "int", "true", true);
        $this->addColumn("biennio", TITLE_BIENNIUM, "50", "int", "true", true);
        $this->addColumn("dt_superamento", TITLE_DT_PASSED, "200", "datetime", "true", true);
        $this->addColumn("dt_ins", TITLE_DT_INS, "200", "datetime", "true", false);
        $this->addColumn("dt_mod", TITLE_DT_MOD, "200", "datetime", "true", true);

        $this->sqlselect = "SELECT a.* ";
        $this->sqlfrom = "FROM $this->dbtablename a ";
    }
    
}

