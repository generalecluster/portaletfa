<?php

/**
 * Copyright 2013 Softmasters di Alex Laudani
 * http://www.softmasters.net
 * 
 * Il presente file fa parte dell'applicativo gestionale concesso in licenza al
 * IV Centro di Mobilitazione del Corpo Militare della Croce Rossa Italiana. 
 * 
 * Sono vietati l'utilizzo da parte di terzi e la redistribuzione a terzi 
 * dell'applicativo o di parti di esso a qualsiasi titolo, oneroso o gratuito, 
 * senza il consenso scritto dell'autore.
 * 
 */

require_once 'DbBaseObject.php';

class CorrelationEntity extends DbBaseObject {
    public function __construct($dbtablename, $key1, $key2, $table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id,$pager_id);
        $this->dbtablename = $dbtablename;
        $this->addColumn("id_".$dbtablename, "ID", "50", "int", "true", false);
        $this->addColumn($key1, $key1, "20", "int", "true", true);
        $this->addColumn($key2, $key2, "20", "int", "true", true);
    }
}
?>
