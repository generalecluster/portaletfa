<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
require_once 'autoload.php';

class Utente extends DbBaseObject {
    
    public function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id, $pager_id);
        $this->dbtablename = 'utente';
        $this->addColumn("id_".$this->dbtablename, "ID", "50", "int", "true", false);
        $this->addColumn("username", TITLE_USERNAME, "200", "string", "true", true);
        $this->addColumn("password", TITLE_PASSWORD, "200", "string", "true", true);
        $this->addColumn("nome", TITLE_NAME, "200", "string", "true", true);
        $this->addColumn("cognome", TITLE_LASTNAME, "200", "string", "true", true);
        $this->addColumn("dt_nascita", TITLE_DATE_OF_BIRTH, "200", "datetime", "true", true);
        $this->addColumn("id_comune_nascita", "id_Comune_nascita", "200", "int", "true", true);
        $this->addColumn("id_sex", TITLE_SEX , "200", "int", "true", true);
        $this->addColumn("codicefiscale", TITLE_SSN_NUMBER, "200", "string", "true", true);
        $this->addColumn("email", TITLE_EMAIL, "200", "string", "true", true);
        $this->addColumn("id_tipoformazione", TITLE_TRAINING_TYPE, "200", "int", "true", true);
        $this->addColumn("biennio", LABEL_BIENNIUM, "50", "int", "true", true);
        $this->addColumn("descrizione_tipoformazione", TITLE_TRAINING_TYPE, "200", "string", "true", false);
        $this->addColumn("codicerui", TITLE_RUI_CODE, "200", "string", "true", true);
        $this->addColumn("cod_agenzia", TITLE_AGENCY_CODE, "200", "string", "true", true);
        $this->addColumn("cod_das", TITLE_DAS_CODE, "200", "string", "true", true);
        $this->addColumn("id_tipoutente", TITLE_USERTYPE, "200", "int", "true", true);
        $this->addColumn("descrizione_tipoutente", TITLE_USERTYPE, "200", "string", "true", false);
        $this->addColumn("id_ruolo", TITLE_ROLE, "20", "int", "true", true);
        $this->addColumn("id_stato", TITLE_STATUS, "200", "int", "true", true);
        $this->addColumn("fl_privacy", "fl_privacy", "20", "int", "true", true);
        $this->addColumn("descrizione_stato", TITLE_STATUS, "200", "string", "true", false);
        $this->addColumn("token", "token", "200", "string", "true", true);
        $this->addColumn("tokentime", "tokentime", "200", "datetime", "true", true);
        $this->addColumn("pwdtoken", "pwdtoken", "200", "string", "true", true);
        $this->addColumn("pwdtokentime", "pwdtokentime", "200", "datetime", "true", true);
        $this->addColumn("dt_ins", TITLE_DT_INS, "200", "datetime", "true", false);
        $this->addColumn("dt_mod", TITLE_DT_MOD, "200", "datetime", "true", true);

        $this->sqlselect = "SELECT a.*, b.descrizione_tipoformazione, c.descrizione_tipoutente, d.descrizione_stato ";
        $this->sqlfrom = "FROM $this->dbtablename a "
                . "       LEFT JOIN tipoformazione b ON a.id_tipoformazione=b.id_tipoformazione "
                . "       LEFT JOIN tipoutente c ON a.id_tipoutente=c.id_tipoutente "
                . "       LEFT JOIN stato d ON a.id_stato=d.id_stato" ;
    }
    
}

