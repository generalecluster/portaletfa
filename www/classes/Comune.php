<?php
/*
 * Questo file è stato creato il 15-feb-2013 da Alex Laudani, Softmasters
 * per il committente Coretech S.r.l.
 * Il presente software è concesso in licenza d'uso a Coretech S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
require_once 'DbBaseObject.php';

class Comune extends DbBaseObject {

    function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id,$pager_id);
        $this->dbtablename = "comune";
        //$this->setOption("jsonReader", '{id: "id_proprietario"}');
        $this->setOption("url", 'do_comune.php');
        $this->setOption("editurl", 'do_comune.php');
        //$this->setOption("rowId", 'id_proprietario');
        $this->addColumn("id_comune", TITLE_CITY, "50", "string", "true", false);
        $this->addColumn("nome", TITLE_NAME, "200", "string", "true", true);
        $this->addColumn("regione", TITLE_REGION, "200", "string", "true", true);
        $this->addColumn("fl_capoluogo_prov", "Capoluogo", "20", "int", "true", true);
        $this->addColumn("id_provincia", TITLE_PROVINCE_ID, "20", "int", "true", true);
        $this->addColumn("cod_catasto", "Cod. catasto", "40", "string", "true", true);
        $this->addColumn("cod_alfanumerico", "Cod. alfanum.", "40", "string", "true", true);
        $this->addColumn("cod_numerico", "Cod. num.", "40", "string", "true", true);
        $this->addColumn("popolazione", TITLE_POPULATION, "40", "int", "true", true);
        $this->options['colModel']['id_provincia']->hidden = true;
        //$this->options['colModel']['id_collana']->editoptions['maxlength'] = 3;
        //$this->options['colModel']['descrizione']->editoptions['size'] = 50;
        //$this->options['colModel']['buttonscol']->retrieve = false;
        $this->editButton = true;
        $this->deleteButton = true;
        $this->addButton = true;
        //$this->sqlselect = "SELECT * ";
        /*$this->sqlfrom = "FROM proprietario a LEFT JOIN tipo_societa b ON a.id_tipo=b.id LEFT JOIN provincia c ON
            a.id_prov=c.id LEFT JOIN comune d on a.id_comune_nascita=d.id LEFT JOIN provincia e ON
            a.id_prov_nascita=e.id LEFT JOIN agenzia f ON a.id_agenzia=f.id ";

        */
    }

    function renderNavigator() {
        $ret = "";
        $myjs = <<<EOD
            \$.jgrid.nav.addfunc = function() {location='nuovo_proprietario.php';};
            \$.jgrid.nav.editfunc =
                    function(id) {
                        if (id!='') {
                            location='modifica_proprietario.php?id='+id;
                        }
                    };

EOD;
        $ret .= $myjs;
        $ret .= "\n";
        $ret .= parent::renderNavigator();
        return $ret;

    }

    

 }

?>