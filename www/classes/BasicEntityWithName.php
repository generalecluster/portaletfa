<?php
require_once 'DbBaseObject.php';
require_once 'BasicEntity.php';

class BasicEntityWithName extends BasicEntity {
    public function __construct($dbtablename, $table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($dbtablename,$table_id,$pager_id);
        $this->dbtablename = $dbtablename;
        $this->addColumn("nome_".$dbtablename, "Nome", "200", "string", "true", true);
    }
    
}
?>
