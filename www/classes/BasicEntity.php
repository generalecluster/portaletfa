<?php
require_once 'DbBaseObject.php';

class BasicEntity extends DbBaseObject {
    public function __construct($dbtablename, $table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id,$pager_id);
        $this->dbtablename = $dbtablename;
        $this->addColumn("id_".$dbtablename, "ID", "50", "string", "true", false);
        $this->addColumn("descrizione_".$dbtablename, "Descrizione", "200", "string", "true", true);
        
        $this->sqlselect = "SELECT * ";
        $this->sqlfrom = "FROM $dbtablename ";
    }
    
}
?>
