<?php
/*
 * Questo file è stato creato il 05 Ott 2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

class Config {
    //A pdo connection string for the database.
    public $dbConnstring = 'mysql:host=127.0.0.1;dbname=vittoria;';
    //The username of the databse user
    public $dbUser = 'yourusername';
    //The password of the database user
    public $dbPass = 'yourpassword';
    //define('DOCPATH','allegati/');
    //The default language to be used in the language configuration.
    public $lang = 'it';
    
    /**
     * Loads the language file corresponding to the language code set in the $lang class member.
     */
    public function loadLanguage() {
        require_once("lang/lang_$this->lang.php");
    }
}
?>
