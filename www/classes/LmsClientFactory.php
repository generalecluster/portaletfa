<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LmsClientFactory {
    private static $instance = null;
    
    public static function getInstance() {
        if (self::$instance == null) {
            $instance = new MoodleClient();
        }
        return $instance;
    }
}