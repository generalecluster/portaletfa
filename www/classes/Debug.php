<?php
/* 
 * Questo file è stato creato il 8-giu-2010 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a Vittoria Assicurazioni.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

class Debug {

    public $verbose = true;

    function out($arg) {
        if ($this->verbose)
            echo $arg;
    }

    function outln($arg) {
        if ($this->verbose)
            echo $arg."<br/>";
    }

}
?>
