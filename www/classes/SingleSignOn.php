<?php
/**
 * Questo file è stato creato il 05 Ott 2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
require_once 'autoload.php';
class SingleSignOn {
    
    /**
     *
     * @var String The remote host to which the user is requesting access. 
     * This should not include the protocol part (http:// or https://)
     */
    public $host;
    
    /**
     *
     * @var String The username of the user requesting access to the remote host.
     */
    public $username;
    
    /**
     *
     * @var String The URL where the user should be redirected after authentication.
     */
    public $url;
    
    public function __construct($host, $username, $courseid) {
        $this->host = $host;
        $this->username = $username;
        $this->url = $url;
        $this->courseid = $courseid;
        error_log("constructed SingleSignOn with username ".$this->username);
    }
    
    public function prepareToken($username, $now, $secret) {
        $token = hash('sha256', $username.$now.$secret, false);
        return $token;
    }
    
    /**
     + Pull authentication request from remote host.
     * This function sends a token to the remote host. The host must reply with 
     * the token, the name of the application which is asking the user data, 
     * and the ip address of the host that has made the request.
     */
    public function authenticate() {
        $allowedhost = new AllowedHost();
        $allowedhosts = $allowedhost->getBy(array('host'=>$this->host));
        if (count($allowedhosts) > 0) {
            //check if user is enrolled into course
            $db = new DbBaseObject('', '');
            $db->sqlselect = "SELECT id_utente_unitadidattica ";
            $db->sqlfrom = "FROM utente_unitadidattica a LEFT JOIN utente b ".
                    " ON a.id_utente = b.id_utente LEFT JOIN unitadidattica c".
                    " ON a.id_unitadidattica = c.id_unitadidattica ";
            $enrolments = $db->getBy(array('username' => $this->username, 'id_extern' => $this->courseid));
            //echo "<br>enrolments. username:".$this->username." - course:". $this->courseid ." ".print_R($enrolments,1);die();
            $userallowedtocourse = false;
            if (is_array($enrolments)) {
                if (count($enrolments) == 1) {
                    $userallowedtocourse = true;
                }
            }
            //echo "userallowed:".print_r($userallowedtocourse,1);die();
            if (!$userallowedtocourse) {
                throw new Exception(ERROR_USER_NOT_SUBSCRIBED_COURSE. " idc: ".$this->courseid." - usr: ".$this->username);
            }
            $now = date("Y-m-d H:i:s");
            $token = $this->prepareToken($this->username, $now, $allowedhosts[0]->secret);
            $authrequest = new AuthRequest();
            $authrequest->host = $this->host;
            $authrequest->token = $token;
            $authrequest->dt_ins = $now;
            $authrequest->username = $this->username;
            $authrequest->insert();
            header("Location: http://".$this->host.$allowedhosts[0]->authpage."?key=".urlencode(base64_encode($token))."&url=".urlencode($this->url));
        } else {
            throw new Exception(ERROR_HOST_NOT_ALLOWED);
        }
    }
    
    /**
     * Checks the response from the external service requiring user authentication.
     * The external service has to provide some parameters to verify its identity and
     * to check the user login status.
     * 
     * @param string $key The key originally sent by the authentication provider
     * @param string $appname A string containing the name of the application to which the user is requesting access
     * @param type $ipforwarded
     * @param type $ipremote
     * @return -1 if more than 30 seconds have elapsed from the start of the transaction,
     * -2 if 
     */
    public function checkLogin($key, $appname, $ipforwarded, $ipremote) {
        //error_log("entered checklogin $$$$$$$$$$$$$$$$$$$$$$");
        $retval = new stdClass();
        $retval->code = 0;
        //echo ("key: ". base64_decode($key));
        //retrieve authorization request data from the database, related to the 
        //token passed by the remote server.
        $authrequest = new AuthRequest();
        $authrequests = $authrequest->getBy(array('token'=>base64_decode($key)));
        if  (count($authrequests) > 0 ) {
            //check if token has expired. Expiration time is 30 seconds.
            $tokendate = $authrequests[0]->dt_ins;
            $elapsed = StrUtils::dateDifference($tokendate, date('Y-m-d H:i:s'), '%s');
            //echo "elapsed:".$elapsed;
            //error_log("ELAPSED: ".$elapsed);
            if ($elapsed > 30) {
                $retval->code = -1; //token expired
            } else {
                //if token has not expired, check if the remote host is in the 
                //list of allowed hosts on the database.
                $allowedhost = new AllowedHost();
                $allowedhosts = $allowedhost->getBy(array('host'=>$this->host));
                if (count($allowedhosts) > 0) {
                    $retval->code = 0;
                    $retval->dt_ins = $tokendate;
                    $retval->username = $authrequests[0]->username;
                } else {
                    $retval->code = -3; //host not allowed
                }
            }
        } else {
            $retval->code = -2; //invalid token
        }
        return $retval;
    }
    
}
