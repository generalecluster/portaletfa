<?php
/* 
 * Questo file è stato creato il 5-mag-2010 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

require_once 'DBconn.php';

class Object{};

/**
 * Basic class representing a database entity.
 * 
 * The DbBaseObject class is a class implementing some basic functions used to
 * map a php object to a database table.
 * A class representing a database table will extend the DbBaseObject class.
 * The extending class must define the structure of the underlying table using the
 * addColumn() method multiple times if needed.
 * Then, the extending class may define the "SELECT" part of the query using 
 * the "sqlselect" property and then the "FROM" part using the "sqlfrom" property.
 */
class DbBaseObject {

    
    public static $PREFIX_DESCRIPTION = "descrizione_";
    /**
     *
     * @var string The name of the related database table
     */
    public $dbtablename = "";
    
    public $html_table_id = "";
    
    /**
     *
     * @var string The "SELECT" part of the SQL query used to retrieve the object data.
     * 
     * Example: $this->$sqlselect = "SELECT id, description ";
     */
    public $sqlselect = "";
    
    /**
     *
     * @var string The "FROM" part of the SQL query used to retrieve the object data.
     * 
     * Example: $this->sqlfrom = "FROM users a LEFT JOIN orders b ON b.id_user = a.id ";
     */
    public $sqlfrom = "";
    
    /**
     *
     * @var string The "WHERE" part of the SQL query used to retrieve the object data.
     *
     * Example: $this->sqlwhere = "WHERE users a LEFT JOIN orders b ";
     */
    public $sqlwhere = "";
    
    /**
     *
     * @var string The "GROUP BY" part of the SQL query used to retrieve the object data.
     *
     * Example: $this->sqlgroupby = "GROUP BY id";
     */
    public $sqlgroupby = "";
    
    /**
     *
     * @var string The "ORDER BY" part of the SQL query used to retrieve the object data.
     *
     * Example: $this->sqlorderby = "ORDER BY price DESC ";
     */
    public $sqlorderby = "";
    
    /**
     * @var int An integer representing the row where to start collecting data from.
     */
    public $start = 0;
    
    /**
     * @var int An integer representing the number of rows to fetch.
     */
    public $pagesize = 0;
    
    public $options = array();
    public $editButton = false;
    public $deleteButton = false;
    public static $class;
    public $sqlparams = array();

    /**
     * This constructor is used for interaction with the javascript library jqtable
     * 
     * If jqtable is not being used, the params may be left empty.
     * 
     * @param type $table_id The html id of the table where data are to be displayed
     * @param type $pager_id The html id of the div where the pagination controls will be displayed
     */
    function __construct($table_id, $pager_id) {
        $this->html_table_id = $table_id;
        $this->setOption("datatype", 'json');
        $this->setOption("pager", '#'.$pager_id);
        $this->setOption("sortorder", 'asc');
        $this->setOption("viewrecords", true);
        $this->setOption("rowNum", '3');
        $this->setOption("rowList", array(30,50,100));
        $this->setOption("rowNum", 50);
        $this->setOption("height",'auto');
        $this->options['edit']['width'] = 'auto';
        //$this->pagesize = bcsub(bcpow(bcpow("2","32"),"2"),"1");
        $this->pagesize = PHP_INT_MAX;
    }

    final public static function getInstance() {
        static $aoInstance = array();

        $calledClassName = get_called_class();

        if (! isset ($aoInstance[$calledClassName])) {
            $aoInstance[$calledClassName] = new $calledClassName();
        }

        return $aoInstance[$calledClassName];
    }

    /**
     * Adds a database column mapping to the model of the class
     * 
     * Use this method to add a mapping between a database table column and a 
     * property of the class.
     * 
     * @param type $column_id The name of the column in the database table
     * @param type $title The name of the field as it will be displayed in the html table
     * @param type $width The width in pixels of the html column
     * @param type $sorttype The data type used for sorting. Possible values are int, string, date, datetime, currency.
     * @param type $sortable If true, the html column header may be clicked for sorting
     * @param type $editable If true, the field may be edited in the html table
     */
    function addColumn($column_id, $title, $width, $sorttype, $sortable, $editable=false) {
        $column = new Object();
        $column->name = $column_id;
        $column->title = $title;
        $column->index = $column_id;
        $column->width = $width;
        $column->sorttype = $sorttype;
        $column->sortable = $sortable;
        $column->editable = $editable;
        $column->retrieve = TRUE;
        $this->options["colModel"][$column_id] = $column;
        $this->options["colNames"][$column_id] = $title;
    }

    function getOption($option_name) {
        return $this->options["$option_name"];
    }

    function setOption($option_name,$option_value) {
        $this->options["$option_name"] = $option_value;
    }

    function getJSONData() {
        return json_encode($this->getData());
    }
    
    function getData() {
        //bcscale(0);//this is needed to remove the trailing zeroes from the pagesize
        $response = new stdClass();
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page="";
        }
        $sord = "";
        $total_pages = "";
        // get the requested page
        if (isset($_GET['rows'])) { $limit = $_GET['rows']; }
        //get how many rows we want to have into the grid
        if (isset($_GET['sidx'])) { $sidx = $_GET['sidx']; }
        // get index row - i.e. user click to sort
        if (isset($_GET['sord'])) { $sord = $_GET['sord']; }
        // get the direction
        if(!isset($sidx)) $sidx =1;
        // connect to the database
        $conn = DBConn::getConn();
        if ($this->sqlfrom == "") {
            $query = "SELECT COUNT(*) AS count FROM ".$this->dbtablename." ".$this->sqlwhere;
        } else {
            $query = "SELECT COUNT(*) AS count ".$this->sqlfrom." ".$this->sqlwhere;
        }
        $stmt = $conn->prepare($query);
        //print_r($this->sqlparams);
        foreach ($this->sqlparams as $key => $val) {
            //echo "binding ".$key." : ".$val."<br/>";
            $stmt->bindValue($key, $val);
        }
        //echo $query;
        $stmt->execute();
        
        $row = $stmt->fetch();
        $count = $row['count'];
//        //echo $count;
//        if (!isset($_GET['page'])) $page = 1;
//        
//        if (!isset($_GET['rows'])) $limit = $count;
//        if( $count >0 ) {
//            $total_pages = ceil($count/$limit);
//        } else {
//            $total_pages = 0;
//        }
//        if ($page > $total_pages) $page=$total_pages;
//        $start = $limit*$page - $limit; // do not put $limit*($page - 1)
//        if ($start < 0) $start = 0;
        if ($this->sqlselect == "") {
            $SQL = "SELECT ";
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $SQL .= $col_id.", ";
                }
            }
            $SQL = substr($SQL, 0, strlen($SQL)-2);
            //echo "sqlwhere= ".$this->sqlwhere."\n";
            $SQL .= " FROM ".$this->dbtablename." ".$this->sqlwhere." ".$this->sqlgroupby;
            if (!empty($this->sqlorderby)) {
                $SQL .= $this->sqlorderby;
            }
            $SQL .= " LIMIT ".$this->start." , ".$this->pagesize;
        } else {
            $SQL = $this->sqlselect." ".$this->sqlfrom." ".$this->sqlwhere." ".$this->sqlgroupby;
            if (!empty($this->sqlorderby)) {
                $SQL .= $this->sqlorderby;
            }
            $SQL .= " LIMIT ".$this->start." , ".$this->pagesize;
        }
        //echo $SQL;
        $stmt = $conn->prepare($SQL);
        foreach ($this->sqlparams as $key => $val) {
            $stmt->bindValue($key, $val);
        }
        $stmt->execute();
        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;
        $response->rows = array();
        $i=0;
        while($row = $stmt->fetch()) {
            //print_r($row);
            //$response->rows[$i]['id']=$row[0];
            //$response->rows[$i]['cell']=array_slice($row,1);
            $tempobj = new stdClass();
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $tempobj->$col_id = $row[$col_id];
                }
            }
            $response->rows[$i] = $tempobj;
            $i++;
        }
        $conn = null;
        //error_log(print_r($response->rows,1));
        return $response;
    }
    
    function getDataOld() {
        $response = new stdClass();
        if (isset($_GET['page'])) $page = $_GET['page'];
        $sord = "";
        // get the requested page
        if (isset($_GET['rows'])) { $limit = $_GET['rows']; }
        //get how many rows we want to have into the grid
        if (isset($_GET['sidx'])) { $sidx = $_GET['sidx']; }
        // get index row - i.e. user click to sort
        if (isset($_GET['sord'])) { $sord = $_GET['sord']; }
        // get the direction
        if(!isset($sidx)) $sidx =1;
        // connect to the database
        $conn = DBConn::getConn();
        if ($this->sqlfrom == "") {
            $query = "SELECT COUNT(*) AS count FROM ".$this->dbtablename." ".$this->sqlwhere;
        } else {
            $query = "SELECT COUNT(*) AS count ".$this->sqlfrom." ".$this->sqlwhere;
        }
        $stmt = $conn->prepare($query);
        //print_r($this->sqlparams);
        foreach ($this->sqlparams as $key => $val) {
            //echo "binding ".$key." : ".$val."<br/>";
            $stmt->bindValue($key, $val);
        }
        //echo $query;
        $stmt->execute();
        
        $row = $stmt->fetch();
        $count = $row['count'];
        //echo $count;
        if (!isset($_GET['page'])) $page = 1;
        
        if (!isset($_GET['rows'])) $limit = $count;
        if( $count >0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) $page=$total_pages;
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)
        if ($start < 0) $start = 0;
        if ($this->sqlselect == "") {
            $SQL = "SELECT ";
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $SQL .= $col_id.", ";
                }
            }
            $SQL = substr($SQL, 0, strlen($SQL)-2);
            //echo "sqlwhere= ".$this->sqlwhere."\n";
            $SQL .= " FROM ".$this->dbtablename." ".$this->sqlwhere." ".$this->sqlgroupby." ORDER BY $sidx $sord LIMIT $start , $limit";
        } else {
            $SQL = $this->sqlselect." ".$this->sqlfrom." ".$this->sqlwhere." ".$this->sqlgroupby." ORDER BY $sidx $sord LIMIT $start , $limit";
        }
        echo $SQL;
        $stmt = $conn->prepare($SQL);
        foreach ($this->sqlparams as $key => $val) {
            $stmt->bindValue($key, $val);
        }
        $stmt->execute();
        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;
        $response->rows = array();
        $i=0;
        while($row = $stmt->fetch()) {
            //print_r($row);
            //$response->rows[$i]['id']=$row[0];
            //$response->rows[$i]['cell']=array_slice($row,1);
            $tempobj = new stdClass();
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $tempobj->$col_id = $row[$col_id];
                }
            }
            $response->rows[$i] = $tempobj;
            $i++;
        }
        $conn = null;
        //error_log(print_r($response->rows,1));
        return $response;
    }

    function renderJqOptions() {
        //$this->options["colNames"] = array_values($this->options["colNames"]);
        //$this->options["colModel"] = array_values($this->options["colModel"]);
        $optionsclone = $this->options;
        $optionsclone["colNames"] = array_values($this->options["colNames"]);
        $optionsclone["colModel"] = array_values($this->options["colModel"]);
        //echo json_encode($optionsclone);
        $ret = "jqoptions = ".json_encode($optionsclone).";\n";
        $ret .= "jqoptions['loadError'] = function(xhr,status,error){alert('errore:'+xhr.responseText);};";
        return $ret;
    }
    
    function beforeRenderGrid() {
        //abstract
    }

    function renderGrid() {
        $ret = "$.jgrid.edit.width = 'auto';\n";
        $ret .= "jQuery(\"#$this->html_table_id\").jqGrid(jqoptions);\n";
        //$ret .= "jQuery(\"#$this->html_table_id\").jqGrid('navGrid',".$this->options['pager'].",{edit:false,add:false,del:false});";
        return $ret;
    }

    function beforeRenderNavigator() {
    
    }

    function renderNavigator() {
        $aftercomplete = <<<EOD
            function(response,postdata,formid) {
                if (trim(response.responseText) == 'OK') {
                    alert("Operazione completata");
                } else {
                    alert("Errore: " + response.responseText);
                }
            }
EOD;
        $ret = "jQuery(\"#$this->html_table_id\").jqGrid('navGrid','".$this->options['pager']."',{edit:".json_encode($this->editButton).",add:".json_encode($this->addButton).",del:".json_encode($this->deleteButton).",search:false},{afterComplete:$aftercomplete,closeAfterEdit:true},{afterComplete:$aftercomplete,closeAfterAdd:true},{afterComplete:$aftercomplete});\n";
        return $ret;
    }

    function afterRenderNavigator() {
        
    }

    function renderJavascript() {
        $ret = $this->renderJqOptions();
        $ret .= "\n";
        $ret .= $this->beforeRenderGrid();
        $ret .= "\n";
        $ret.= $this->renderGrid();
        $ret .= "\n";
        $ret .= $this->beforeRenderNavigator();
        $ret .= "\n";
        $ret .= $this->renderNavigator();
        $ret .= "\n";
        $ret .= $this->afterRenderNavigator();
        $ret .= "\n";
        return $ret;

    }

    function delete($id) {
        $keys = array_keys($this->options["colNames"]);
        $primary_key = $keys[0];
        $query = "DELETE FROM ".$this->dbtablename;
        if ($this->sqlwhere == "") {
            $query .= " WHERE ".$primary_key."='".$id."'";

        } else {
            $query .= $this->sqlwhere;
        }
        //error_log("*******query:".$query);
        $conn = DBConn::getConn();
        $conn->query($query);
        $conn = null;
    }

    function update() {
        $query = "UPDATE ".$this->dbtablename." SET ";
        foreach ($this->options["colModel"] as $col_id => $column) {
            if ($column->editable && isset($this->$col_id)) {
                $query .= " ".$col_id." = :".$col_id.", ";
            }
        }
        //remove trailing comma
        $query = substr($query, 0, strlen($query)-2);
        //find the name of the primary key (primary key is intended to be the first column in the colmodel)
        $keys = array_keys($this->options["colNames"]);
        $primary_key = $keys[0];
        ////complete query with where clause
        if ($this->sqlwhere != "") {
            $query .= " ".$this->sqlwhere." ";
        } else {
            $query .= " WHERE ".$primary_key." = '".$this->$primary_key."'";
        }
        //echo $query;
        try {
            $conn = DBConn::getConn();
            //echo $query;
            $stmt = $conn->prepare($query);
            foreach ($this->options["colModel"] as $col_id => $column) {
                if ($column->editable && isset($this->$col_id)) {
                    $colvalue = "";
                    if ($column->sorttype == "date") {
                        //convert from italian d-m-Y date to mysql Y-m-d date
                        //$colvalue = date('Y-m-d', strtotime($this->$col_id));
                        $colvalue = DBConn::italianToMysqlDate($this->$col_id);
                    } elseif ($column->sorttype == "datetime") {
                        $colvalue = DBConn::italianToMysqlDateTime($this->$col_id);
                    } elseif ($column->sorttype == "currency") {
                        $colvalue = str_replace(",",".",$this->$col_id);
                    } else {
                        $colvalue = $this->$col_id;
                    }
                    $stmt->bindValue(":$col_id",$colvalue);
                }
            }
            $stmt->execute();
            $conn = null;
        } catch (PDOException $e) {
            $conn = null;
            throw ($e);
        }
        
    }

    function insert() {
        $query = "INSERT INTO ".$this->dbtablename."(";
        foreach ($this->options["colModel"] as $col_id => $column) {
            if ($column->editable && isset($this->$col_id)) {
                $query .= " ".$col_id.", ";
            }
        }
        //remove trailing comma
        $query = substr($query, 0, strlen($query)-2);
        $query .= ") VALUES(";
        foreach ($this->options["colModel"] as $col_id => $column) {
            if ($column->editable && isset($this->$col_id)) {
                $query .= ":".$col_id.", ";
            }
        }
        //remove trailing comma
        $query = substr($query, 0, strlen($query)-2);
        $query .= ")";
        
        $conn = DBConn::getConn();
        $stmt = $conn->prepare($query);

        foreach ($this->options["colModel"] as $col_id => $column) {
            if ($column->editable && isset($this->$col_id)) {
                $colvalue = "";
                if ($column->sorttype == "date") {
                    //convert from italian d-m-Y date to mysql Y-m-d date
                    //$colvalue = date('Y-m-d', strtotime($this->$col_id));
                    $colvalue = DBConn::italianToMysqlDate($this->$col_id);
                } elseif ($column->sorttype == "datetime") {
                        $colvalue = DBConn::italianToMysqlDateTime($this->$col_id);
                } elseif ($column->sorttype == "currency") {
                    $colvalue = str_replace(",",".",$this->$col_id);
                } elseif ($column->sorttype == "int") {
                    //error_log("column value: ".$this->$col_id." to col ".$col_id);
                    $colvalue = ($this->$col_id === "" ? null : $this->$col_id);
                    //error_log("binding value: ".$colvalue." to col ".$col_id);
                } else {
                    $colvalue = $this->$col_id;
                }
                $stmt->bindValue($col_id, $colvalue);
            }
        }
        //echo $query;
        try {
            $stmt->execute();
            $ret = $conn->lastInsertId();
            $conn = null;
            return $ret;
        } catch (PDOException $e) {
            $conn = null;
            throw $e;
        }
    }

    function getByPrimaryKey($id) {
        $ret = "";
        $keys = array_keys($this->options["colNames"]);
        $primary_key = $keys[0];
        //$query = "SELECT * FROM ".$this->dbtablename." WHERE ".$primary_key." = '".$id."'";
        $query = $this->sqlselect.$this->sqlfrom. " WHERE ".$primary_key." = '".$id."'";
        $conn = DBConn::getConn();
        $row = $conn->query($query)->fetch();
        if (is_array($row)) {
            foreach ($row as $col => $value) {
                //echo "col: ".$col." - val:".$value."<br/>";
                $this->$col = $value;
            }
        }
    }

    function getPlainJson() {
        // connect to the database
        $conn = DBConn::getConn();
        if ($this->sqlselect == "") {
            $SQL = "SELECT ";
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $SQL .= $col_id.", ";
                }
            }
            $SQL = substr($SQL, 0, strlen($SQL)-2);
            //echo "sqlwhere= ".$this->sqlwhere."\n";
            $SQL .= " FROM ".$this->dbtablename." ".$this->sqlwhere;
        } else {
            $SQL = $this->sqlselect." ";
            if ($this->sqlfrom == "") {
                $SQL .= " FROM ".$this->dbtablename." ";
            } else {
                $SQL .= " ".$this->sqlfrom." ";
            }
            $SQL .= " ".$this->sqlwhere;
        }
        //echo $SQL;
        $SQL .= $this->sqlorderby;
        $result = $conn->query($SQL);
        $i=0;
        $response = array();
        //add row fields to response in the same order as they are listed in the class constructor.
        while($row = $result->fetch()) {
            foreach ($this->options['colModel'] as $col_id => $column) {
                if ($column->retrieve !== false) {
                    $response[$i][$col_id] = $row[$col_id];
                }
            }
            $i++;
        }
        $conn = null;
        return json_encode($response);
    }
    
    function renderJson() {
        $result = new stdClass();
        foreach ($this->options['colModel'] as $key => $val) {
            $result->$key = $this->$key;
        }
        return json_encode($result);
    }

    /**
     * Retrieve one or more records from a table searching by columns passed in an array
     * @param Array $params an associative array containing <column name>,<value> pairs.
     * @return Array an array of Objects, each one representing a record in the table.
     */
    function getByold($params) {
        // connect to the database

        $conn = DBConn::getConn();
        if ($this->sqlselect == "") {
            $SQL = "SELECT ";
            foreach ($this->options['colModel'] as $col_id => $column) {
                if (!isset($column->retrieve) || $column->retrieve !== false) {
                    $SQL .= $col_id.", ";
                }
            }
            $SQL = substr($SQL, 0, strlen($SQL)-2);
            //echo "sqlwhere= ".$this->sqlwhere."\n";
        } else {
            $SQL = $this->sqlselect." ";
        }

        if ($this->sqlfrom == "")
            $SQL .= " FROM ".$this->dbtablename." ";
        else
            $SQL .= " ".$this->sqlfrom." ";

        if ($this->sqlwhere == "") {
            if (is_array($params) && count($params) > 0) {
                $SQL .= " WHERE ";
            }
        } else {
            $SQL .= " ".$this->sqlwhere." ";
            if (is_array($params) && count($params) > 0) $SQL .= " AND ";
        }

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $value) {
                $SQL .= " $key=:$key AND ";
            }
            $SQL = substr($SQL, 0, strlen($SQL)-4);
        }

        $SQL .= " ".$this->sqlorderby;
        
        //echo $SQL;
        $stmt = $conn->prepare($SQL);
        
        if (is_array($params) && count($params) > 0) {
                foreach ($params as $key => $value) {
                    $stmt->bindValue(":$key",$value);
                    //echo "binding: ".$key." to ".$value."<br/>";
                }
            }
        $i=0;
        $stmt->execute();
        $response = array();
        
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //echo "row found: ";
            //print_r($row);
            //$temp_obj = new self::$class("","");
            $temp_obj = clone $this;
            foreach ($row as $key => $val) {
                $temp_obj->$key = $val;
            }
            $response[] = $temp_obj;
            //echo $temp_obj->prezzo_vendita;
        }
        $conn = null;
        //print_r($response);
        return $response;
    }
    
    /**
     * Retrieve one or more records from a table searching by columns passed in an array
     * @param Array $params an associative array containing <column name>,<value> pairs.
     * @return Array an array of Objects, each one representing a record in the table.
     */
    function getBy($params) {
        $SQL = "";
        if ($this->sqlwhere == "") {
            if (is_array($params) && count($params) > 0) {
                $SQL .= " WHERE ";
            }
        } else {
            $SQL .= " ".$this->sqlwhere." ";
            if (is_array($params) && count($params) > 0) $SQL .= " AND ";
        }

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $value) {
                $SQL .= " $key=:$key AND ";
            }
            $SQL = substr($SQL, 0, strlen($SQL)-4);
        }
        $tmpwhere = $this->sqlwhere;
        $tmpparams = $this->sqlparams;
        $this->sqlwhere = $SQL;
        //append method params to existing sqlparams
        $this->sqlparams = array_merge($this->sqlparams, $params);
        $response = $this->getData()->rows;
        $this->sqlwhere = $tmpwhere;
        $this->sqlparams = $tmpparams;
        return $response;        
    }

    function test() {
        $conn = DBConn::getConn();
        $value=5;
        $SQL = "SELECT a.description, b.username FROM user_service a LEFT JOIN user b ON a.id_user=b.id_user WHERE b.id_user=:b_id_user";
        echo $SQL;
        $stmt = $conn->prepare($SQL);

        $stmt->bindValue(":b_id_user",$value);

        $i=0;
        $stmt->execute();
        $response = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo $row["description"];
            //echo $temp_obj->prezzo_vendita;
        }
        $conn = null;
    }

    /**
     * Returns a list of options for a select html element
     * @param string $optval the name of the column to be used to populate the option "value" attribute
     * @param string $descr the name of the column to be used to populate the option text.
     * @param string $emptyoptiontext the text of the option for the null value. If this parameter is empty, no option entry is generated for the null value.
     * @return type
     */
    function getList($optval="", $descr="", $selectedval="", $emptyoptiontext="") {
        // connect to the database
        $retval = "";
        $params = $this->sqlparams;
        $conn = DBConn::getConn();
        if ($this->sqlselect == "") {
            $SQL = "SELECT ";
            foreach ($this->options['colModel'] as $col_id => $column) {
                if (!isset($column->retrieve) || $column->retrieve !== false) {
                    $SQL .= $col_id.", ";
                }
            }
            $SQL = substr($SQL, 0, strlen($SQL)-2);
            //echo "sqlwhere= ".$this->sqlwhere."\n";
        } else {
            $SQL = $this->sqlselect." ";
        }

        if ($this->sqlfrom == "")
            $SQL .= " FROM ".$this->dbtablename." ";
        else
            $SQL .= " ".$this->sqlfrom." ";

        if ($this->sqlwhere == "") {
            if (is_array($params) && count($params) > 0) {
                $SQL .= " WHERE ";
            }
        } else {
            $SQL .= " ".$this->sqlwhere." ";
            if (is_array($params) && count($params) > 0) $SQL .= " AND ";
        }

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $value) {
                //PDO does not accept parameter names containing a dot, so the "." has to be replaced by "_".
                $SQL .= " $key=:".str_replace(".", "_", $key)." AND ";
            }
            $SQL = substr($SQL, 0, strlen($SQL)-4);
        }

        $SQL .= " ".$this->sqlorderby;

        //echo $SQL;
        //error_log($SQL);
        $stmt = $conn->prepare($SQL);

        if (is_array($params) && count($params) > 0) {
                foreach ($params as $key => $value) {
                    //PDO does not accept parameter names containing a dot, so the "." has to be replaced by "_".
                    $stmt->bindValue(":".str_replace(".", "_", $key),$value);
                }
            }
        $i=0;
        $stmt->execute();
        $response = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $temp_obj = new self::$class("","");
            foreach ($row as $key => $val) {
                $temp_obj->$key = $val;
            }
            $response[] = $temp_obj;
            //echo $temp_obj->prezzo_vendita;
        }
        $conn = null;
//        echo "tablename:".$this->dbtablename;
//        if ($this->dbtablename == "qualificacivile") {
//            print_r($response);
//        }
        if (($optval == "") && ($descr == "")) {
            return $response;
        } else {
            //create <option> for null value, if required
            if (!empty($emptyoptiontext)) {
                $retval .= '<option value="">'.$emptyoptiontext.'</option>';
            }
            //return a option list for a select.
            foreach ($response as $key2=>$val2) {
                $selstring = "";
                if ($val2->$optval == $selectedval) {
                    $selstring = "selected=\"selected\"";
                }
                //$responselist[] = "<option value=".$value[$optval].">".$value[$descr]."</option>";
                $retval.=  "<option value=\"".$val2->$optval."\" ".$selstring.">".$val2->$descr."</option>";
                //print_R($val2);
            }
            return $retval;
        }
    }

    public function toAssociativeArray() {
        $aResult = array();
        foreach ($this->options['colNames'] as $key => $val) {
            $aResult[$key] = $this->$key;
        }
        return $aResult;
    }

    public function primaryKeyName() {
        $keys = array_keys($this->options["colNames"]);
        $primary_key = $keys[0];
        return $primary_key;
    }

    public function hideColumn($sColname) {
        $this->options["colModel"][$sColname]->hidden = true;
    }
    
    /**
     * Sets the desired columns as hidden.
     * @param array(string) $asColnames a string array containing the names of the columns to hide
     */
    public function hideColumns($asColnames) {
        foreach ($asColnames as $key => $val) {
            $this->options["colModel"][$val]->hidden = true;
        }
    }
    
    public function hideAllColumns() {
        foreach ($this->options["colModel"] as $key => $val) {
            $val->hidden = true;
        }
    }

    public function showColumn($sColname) {
        $this->options["colModel"][$sColname]->hidden = false;
    }
    
    public function showColumns($asColnames) {
        foreach ($asColnames as $key => $val) {
            $this->options["colModel"][$val]->hidden = false;
        }
    }
    
    public function getColumnHtmlType($colname) {
        return $this->options["colModel"][$colname]->htmltype;
    }
    
    public function setColumnHtmlType($colname, $type) {
        $this->options["colModel"][$colname]->htmltype = $type;
    }

    public function setColumnWidth($sColname, $iWidth) {
        $this->options["colModel"][$sColname]->width = $iWidth;
    }

    public function setParams($aParams) {
        $this->sqlparams = $aParams;
    }
    
    /**
     * Merges an array of DbBaseObjects into the form passed as an argument.
     * Each one of the DbBaseObjects will be converted to an associative array 
     * with key => value pairs and the converted array will be appended to the form.
     * If the form already contains an element with the same key, the new value 
     * will be appended without overwriting the existing values.
     * @param type $array
     * @param type $form
     */
    public static function mergeArrayToForm($objlist, &$form) {
        foreach ($objlist as $key => $tempobj) {
            $objarray = $tempobj->toAssociativeArray();
            foreach ($objarray as $arrkey => $arrvalue) {
                $column = $tempobj->options['colModel'][$arrkey];
                if ($column->sorttype=="date") {
                    $form[$arrkey][] = DBConn::mysqlToItalianDate($arrvalue);
                } else {
                    $form[$arrkey][] = $arrvalue;
                }
            }
            //print_r($form);
        } 
    }
    
    /**
     * Retrieve parameter values from the POST of the request and populate the
     * instance of current object with those values.
     * @param type $post an array representing the POST
     */
    public function parsePostValues($post) {
        foreach ($this->options['colModel'] as $col_id => $column) {
            if (!isset($column->retrieve) || $column->retrieve !== false) {
                if (isset($post[$col_id])) {
                    $this->$col_id = $post[$col_id];
                }
            }
        }
    }
    
    /**
     * 
     * @param string[][] $params an array of string arrays containing the list 
     * of ordering columns and the sort direction.
     * Example: 
     * Array
     *  (
     *      [0] => Array
     *      (
     *          [column] => 0
     *          [dir] => asc
     *      )
     *
     *  )
     */
    public function setOrderBy($columns, $orders) {
        if (isset($orders) && is_array($orders) && count($orders)>0) {
            $this->sqlorderby = " ORDER BY ";
        }
        foreach ($orders as $key=>$val) {
            //get name of corresponding column
            $colname = $columns[$val['column']]['name'];
            //get sort direction
            $sortdir = '';
            if ($val['dir'] == 'asc') {
                $sortdir = 'asc';
            } else if ($val['dir'] == 'desc') {
                $sortdir = 'desc';
            }
            //check if passed column exists in the model
            if (isset($this->getOption("colModel")[$colname])) {
                $this->sqlorderby.= " ".$colname." ".$sortdir;
            }
        }
    }
    
    /**
     * execute a insert or update query from a sql string and n array of parameters
     */
    function execSQL($sql, $params) {
        $conn = DBConn::getConn();
        $stmt = $conn->prepare($sql);
        if (is_array($params) && count($params) > 0) {
                foreach ($params as $key => $value) {
                    //PDO does not accept parameter names containing a dot, so the "." has to be replaced by "_".
                    $stmt->bindValue(":".str_replace(".", "_", $key),$value);
                }
            }
        $i=0;
        return $stmt->execute();
    }
}
?>