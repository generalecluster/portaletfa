<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'classes/phpMailer/PHPMailerAutoload.php';
/**
 * Description of Mailer
 *
 * @author alex
 */
class Mailer {
    
    public static $mailserver;
    public static $mailuser;
    public static $mailpass;
    public static $smtpsecure;
    public static $smtpport;
    public static $mailfrom;
    public static $mailfromname;
    public static $instance = null;
    public $mailer;
    
    public function __construct() {
        $config = new Config();
        self::$mailserver = $config->mailserver;
        self::$mailuser = $config->mailuser;
        self::$mailpass = $config->mailpass;
        self::$smtpsecure = $config->smtpsecure;
        self::$smtpport = $config->smtpport; 
        self::$mailfrom = $config->mailfrom;
        self::$mailfromname = $config->mailfromname;
    }
    
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Mailer();
        }
        self::$instance->mailer = new PHPMailer();
        
        $mail = self::$instance->mailer;

        $mail->isSMTP();                        // Set mailer to use SMTP
        $mail->Host = self::$mailserver;        // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                 // Enable SMTP authentication
        $mail->Username = self::$mailuser;      // SMTP username
        $mail->Password = self::$mailpass;      // SMTP password
        $mail->SMTPSecure = self::$smtpsecure;  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = self::$smtpport;          // TCP port to connect to
        $mail->setFrom(self::$mailfrom, self::$mailfromname); // set default sender
        
        return self::$instance;
    }
    
    public function sendMail() {
        $mail = $this->mailer;
        if(!$mail->send()) {
            throw new Exception(ERROR_EMAIL_SENDING." : ".$mail->ErrorInfo);
        }
    }
}
