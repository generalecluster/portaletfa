<?php
/*
 * Questo file è stato creato il 03-ott-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
include_once 'autoload.php';

class AuthRequest extends DbBaseObject {
    
    public function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id, $pager_id);
        $this->dbtablename = 'authrequest';
        $this->addColumn("id_".$this->dbtablename, "ID", "50", "string", "true", false);
        $this->addColumn("host", "host", "200", "string", "true", true);
        $this->addColumn("token", "token", "200", "string", "true", true);
        $this->addColumn("dt_ins", "token date", "200", "datetime", "true", true);
        $this->addColumn("username", "nome utente", "200", "string", "true", true);
    }
    
}

