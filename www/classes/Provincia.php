<?php
/*
 * Questo file è stato creato il 15-feb-2013 da Alex Laudani, Softmasters
 * per il committente Coretech S.r.l.
 * Il presente software è concesso in licenza d'uso a Coretech S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
require_once 'DbBaseObject.php';

class Provincia extends DbBaseObject {

    function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id,$pager_id);
        $this->dbtablename = "provincia";
        //$this->setOption("jsonReader", '{id: "id_proprietario"}');
        $this->setOption("url", 'do_provincia.php');
        $this->setOption("editurl", 'do_provincia.php');
        //$this->setOption("rowId", 'id_proprietario');
        $this->addColumn("id_provincia", "ID provincia", "50", "string", "true", false);
        $this->addColumn("descrizione_provincia", "Descrizione", "200", "string", "true", true);
        $this->addColumn("sigla", "sigla", "20", "string", "true", true);
        $this->addColumn("id_regione", "ID regione", "20", "string", "true", true);
        $this->options['colModel']['id_provincia']->hidden = true;
        //$this->options['colModel']['id_collana']->editoptions['maxlength'] = 3;
        //$this->options['colModel']['descrizione']->editoptions['size'] = 50;
        //$this->options['colModel']['buttonscol']->retrieve = false;
        $this->editButton = true;
        $this->deleteButton = true;
        $this->addButton = true;
        //$this->sqlselect = "SELECT * ";
        /*$this->sqlfrom = "FROM proprietario a LEFT JOIN tipo_societa b ON a.id_tipo=b.id LEFT JOIN provincia c ON
            a.id_prov=c.id LEFT JOIN comune d on a.id_comune_nascita=d.id LEFT JOIN provincia e ON
            a.id_prov_nascita=e.id LEFT JOIN agenzia f ON a.id_agenzia=f.id ";

        */
    }

    function renderNavigator() {
        $ret = "";
        $myjs = <<<EOD
            \$.jgrid.nav.addfunc = function() {location='nuovo_proprietario.php';};
            \$.jgrid.nav.editfunc =
                    function(id) {
                        if (id!='') {
                            location='modifica_proprietario.php?id='+id;
                        }
                    };

EOD;
        $ret .= $myjs;
        $ret .= "\n";
        $ret .= parent::renderNavigator();
        return $ret;

    }

    

 }

?>