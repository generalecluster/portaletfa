<?php
/*
 * Questo file è stato creato il 06-nov-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
require_once 'autoload.php';

class UtenteVm extends Utente {
    
    public function __construct($table_id="", $pager_id="") {
        parent::$class = get_class();
        parent::__construct($table_id, $pager_id);
        $this->dbtablename = 'utente';
        $this->addColumn("piva", TITLE_VAT_NUMBER, "200", "string", "true", true);
        $this->addColumn("id_external", TITLE_ID_EXTERNAL, "200", "int", "true", true);
        $this->addColumn("id_customer", TITLE_ID_CUSTOMER, "200", "string", "true", true);
        $this->addColumn("tos", TITLE_TOS, "50", "boolean", "true", true);
        $this->addColumn("privacy1", TITLE_PRIVACY_1, "50", "boolean", "true", true);
        $this->addColumn("privacy2", TITLE_PRIVACY_2, "50", "boolean", "true", true);
        $this->addColumn("privacy3", TITLE_PRIVACY_3, "50", "boolean", "true", true);
        $this->addColumn("documento_identita", TITLE_IDENTITY_CARD, "200", "string", "true", true);
        
        //$this->sqlselect = "SELECT a.*, b.descrizione_tipoformazione, c.descrizione_tipoutente, d.descrizione_stato ";
        /*
        $this->sqlfrom = "FROM $this->dbtablename a "
                . "       LEFT JOIN tipoformazione b ON a.id_tipoformazione=b.id_tipoformazione "
                . "       LEFT JOIN tipoutente c ON a.id_tipoutente=c.id_tipoutente "
                . "       LEFT JOIN stato d ON a.id_stato=d.id_stato" ;
         */
    }
    
}