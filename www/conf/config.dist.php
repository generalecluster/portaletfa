<?php
/*
 * Questo file è stato creato il 15-feb-2013 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */

class Config {
public $dbConnstring = 'mysql:host=127.0.0.1;dbname=yourdbname;';
public $dbUser = 'yourusername';
public $dbPass = 'yourpassword';
//define('DOCPATH','allegati/');
public $lang = 'it';
}
$lang='it';
//$lang='pt';
//require_once("lang/panel_$lang.php");
?>
