<?php
/*
 * Questo file è stato creato il 04-nov-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = TRUE;
$head = "<script type=\"text/javascript\" src=\"js/learnunit.js\"></script>";
require_once 'autoload.php';
$pagetitle = TITLE_AREA_FORMAZIONE;
include 'header.php';
?>
<h1><?php echo TITLE_AREA_FORMAZIONE?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}
?>
<h2><?php echo LABEL_COURSES_LISTOF?></h2>
<?php
//check if user has subscribed a course and show the subscribe button accordingly
$hassubscribed = false;
$udutente = new UtenteUnitadidattica();
$udutentelist =$udutente->getBy(array("id_utente" => $user->id_utente, "id_unitadidattica" => 1));
if (count($udutentelist) > 0) $hassubscribed = true;
?>
<a href="javascript:void(0)">DAS Difesa in Movimento</a>&nbsp;<button name="btnsubscribe1" id="btnsubscribe1" class="btnsubud<?php echo (!$hassubscribed ? '' : ' hidden')?>" data-ud="1"><?php echo LABEL_COURSE_SUBSCRIBE?></button><a href="http://das.tfalegal.it/course/view.php?id=3" class="btn btn-default btn-lg gocourse<?php echo ($hassubscribed ? '' : ' hidden')?>">Vai al corso</a>
<br/><br/><br/>
<?php
$udutentelist= array();
$hassubscribed = false;
$udutentelist =$udutente->getBy(array("id_utente" => $user->id_utente, "id_unitadidattica" => 3));
if (count($udutentelist) > 0) $hassubscribed = true;
?>
<a href="javascript:void(0)">DAS Difesa Azienda</a>&nbsp;<button name="btnsubscribe1" id="btnsubscribe1" class="btnsubud<?php echo (!$hassubscribed ? '' : ' hidden')?>" data-ud="3"><?php echo LABEL_COURSE_SUBSCRIBE?></button><a href="http://das.tfalegal.it/course/view.php?id=5" class="btn btn-default btn-lg gocourse<?php echo ($hassubscribed ? '' : ' hidden')?>">Vai al corso</a>
<br/><br/><br/>
<!--a href="http://das.tfalegal.it/course/index.php"><?php echo LABEL_COURSES_GOTO?></a-->
<?php
include 'footer.php';
?>