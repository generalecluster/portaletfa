<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$plugins[] = 'CompleteProfilePlugin';
class CompleteProfilePlugin {
    
    public function onAfterLogin() {
        echo "user: ".$_SESSION['user'];
        if (isset($_SESSION['user'])) {
            //check if required fields are missing
            $ut = new Utente();
            $ut->getByPrimaryKey($_SESSION['user']);
            if (!isset($ut->dt_nascita) || empty($ut->dt_nascita)) {
                $_SESSION['messages'][] = MESSAGE_COMPLETE_PROFILE;
                echo "dt_nascita not set, redirecting";
                header('Location: userprofile-edit.php');
            }
        }
    }
    
}