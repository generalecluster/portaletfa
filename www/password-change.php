<?php
/*
 * Questo file è stato creato il 02-feb-2017 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = FALSE;
require_once 'autoload.php';
$pagetitle = TITLE_RECOVER_PASSWORD;
include 'header.php';
?>
<h1><?php echo $pagetitle?></h1>
<?php
if (isset($_SESSION['errors'])) {
    GUI::showErrors($_SESSION['errors']);
    $_SESSION['errors'] = array();
}
if (isset($_GET['t'])) {
    $entity = new Utente();
    $userdata = null;
    $userlist = $entity->getBy(array("pwdtoken" => $_GET['t']));
    if (count($userlist) > 0) {
        $userdata = $userlist[0];
        //check if token has expired
        $tokentime = $userdata->pwdtokentime;
        $now = date(DBConn::$mysqlDateFormat);
        $tokenage = StrUtils::dateDifference($tokentime, $now, '%h');
        //token expires after 24 hours
    } else {
        echo GUI::errorBox(ERROR_WRONG_TOKEN);
    }
} else {
    echo GUI::errorBox(ERROR_PARAMETERS_BAD.": token");
}

if ($tokenage >= 24) {
    echo GUI::errorBox(ERROR_WRONG_TOKEN);
} else {
?>
<h2><?php echo TITLE_CHOOSE_NEW_PASSWORD?></h2>
<form name="frmchangepwd" id="frmchangepwd" method="POST" action="do_utente.php">
    <ul class="wizlist">
        <li>
            <label for="pwd1" class="lbltext"><?php echo LABEL_PASSWORD_NEW?></label>
            <input type="password" id="pwd1" name="pwd1"/>
        </li>
        <li>
            <label for="pwd2" class="lbltext"><?php echo LABEL_PASSWORD_NEW_CONFIRM?></label>
            <input type="password" id="pwd2" name="pwd2"/>
        </li>
        <li>
            <button type="submit"><?php echo LABEL_SEND?></button>
            <input type="hidden" name="action" id="action" value="changepwd"/>
            <input type="hidden" name="t" id="t" value="<?php echo $userdata->pwdtoken?>"/>
        </li>
    </ul>
</form>
<?php
}
include 'footer.php';
?>