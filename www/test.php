<?php
/*
 * Questo file è stato creato il 05-11-2016 da Alex Laudani, Softmasters
 * per il committente TFA & Legal S.r.l.
 * Il presente software è concesso in licenza d'uso a TFA & Legal S.r.l.
 * Il committente e il licenziatario hanno la facoltà di modificare i sorgenti
 * ai fini di solo uso interno. Non sono consentiti la rivendita o
 * la distribuzione in qualsiasi modalità dei sorgenti a terzi, né
 * nella loro forma originale, né in seguito a modifiche apportate, senza il
 * consenso scritto da parte dell'autore.
 * Il presente software utilizza librerie esterne open source, il cui utilizzo
 * è regolato dalle rispettive licenze.
 */
$reserved = FALSE;
require_once 'autoload.php';
$pagetitle = TITLE_REGISTER_PAGE;

$head = "<script type=\"text/javascript\" src=\"js/jquery.steps.js\"></script>";
$head .= "<script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js\"></script>";
$head .= "<link type=\"text/css\" href=\"css/jquery.steps.css\" rel=\"stylesheet\">";
$head .= "<script type=\"text/javascript\" src=\"js/register.js\"></script>";
include 'header.php';
?>
<h1><?php echo $pagetitle?></h1>
<?php
if (isset($_SESSION['errors'])) {
GUI::showErrors($_SESSION['errors']);
$_SESSION['errors'] = array();
}

if (isset($_SESSION['messages'])) {
GUI::showMessages($_SESSION['messages']);
$_SESSION['messages'] = array();
}

$utente = new Utente();
$utente->getByPrimaryKey(24);
?>

<?php
include 'footer.php';
?>
