<?php
spl_autoload_register(function ($class) {
    include 'classes/' . $class . '.php';
});

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$config = new Config();
$config->loadLanguage();
require_once('auth.php');
?>